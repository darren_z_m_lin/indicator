define(["controllers/controllers", "services/services"], function (controllers) {

    controllers.controller("MenuListController", ["$scope", "MenuService","HotKpiService","cache","SearchSharedService","$location", function ($scope, MenuService,HotKpiService,menuCache,sharedService,$location) {
        //console.log("MenuListController is call");
        /**
         * 首页菜单MenuListController
         *
         *
         * -------------History------------------
         * DATE        AUTHOR    VERSION        DESCRIPTION
         * 2013-09-16   蔡书伟   V13.00.001      新增
         *
         *
         * author 蔡书伟
         */

        var menuCacheName ="menuData";
        //初始化数组对象
        var initData = [];
        //初始化cateid属性值
        var initCateId ="9999";
        //初始化获取热门指标数
        var initHotKpiSize = 10;

        var iconImageList = ["icon_1","icon_2","icon_3","icon_4","icon_5","icon_6"]  ;
        var menus = function (item) {
            return {
                id: item.id,
                cssClass:"",
                iconClass:"",
                cateName: item.cateName,
                topCate: item.topCate,
                sonCate:[],
                sonHotKpi:[]
            };
        };
        //更新model
        var changeModel = function()
        {
            $scope.menusData = getMenuModelCache();
        }
        //设置菜单缓存
        var setMenuModelCache = function(menuData)
        {
            menuCache.put(menuCacheName, menuData);
        }
        //获取菜单缓存
        var getMenuModelCache = function()
        {
            //console.info(menuCache.get(menuCacheName));
            return menuCache.get(menuCacheName);
        }


         //初始化父分类数据
        var setInitData = function (responseData) {
             var icon_index =0;
            angular.forEach(responseData, function (item, index) {
                    var menuItem = new menus(item);
                    if((index+1)%2==0)
                    {
                        menuItem.cssClass="graybg";
                    }
                    if(index%6==0)    //超过6个菜单时默然从图标1开始取
                    {
                        icon_index =0;
                    }
                    menuItem.iconClass =iconImageList[icon_index];
                    this.push(menuItem);
                    icon_index++;
            },initData);
               setMenuModelCache(initData);
               return initData;
        }


        //设置子分类数据
        var setSonData = function(parentId,sonObj)
        {
            var tmp_menuData = [];
            angular.forEach(getMenuModelCache(), function (item, index) {
                if (parentId === item.id) {
                    item.sonCate= sonObj.sonCate  ;
                    item.sonHotKpi= sonObj.sonHotKpi  ;
                }
               this.push(item) ;
            },tmp_menuData);
            setMenuModelCache(tmp_menuData);
            //设置子分类数据
            changeModel();
        }

        //检查子分类数据是否存在缓存中
        var isEmptySonData = function(parentId)
        {
            var check = false;
            angular.forEach(getMenuModelCache(), function (item, index) {
                if (parentId === item.id) {
                    if(item.sonCate.length == 0)
                    {
                        return  check = true;
                    }
                }
            });
            return    check;
        }
        //初始化control时获取父类数据
        var initMenuData = function()
        {
            var tmp =  getMenuModelCache() ;
            if(typeof(tmp)==="undefined" )
            {
                MenuService.post({cateId:initCateId}, function (rs) {
                    // alert("sssssssssssss");
                    var menudata  = angular.copy(rs.data);
                    menudata =  setInitData(menudata) ;
                    $scope.menusData =  menudata;
                }, function () {
                });

            }
            else
            {
                  var menudata  =tmp;
                    menudata =  setInitData(menudata) ;
                    $scope.menusData =  menudata;;
            }
        }

        $scope.detail = function(kpiobj) {
            $location.path("detail").search({kpiId:kpiobj.id});
        }

        $scope.search = function(cateobj,menuDom) {

        	 //console.log(cateobj) ;
            angular.element('#'+menuDom).hide();
            var cate = {id:cateobj.id,cateName:cateobj.cateName};
            //console.log(cate) ;
            var message = {event:"MenuListController:Search",msg:cate};
            sharedService.prepForBroadcast(message);
            //sharedService.prepForBroadcast(cateobj);
        }

        $scope.leave = function(menuDom) {

            //console.log(cateobj) ;
            angular.element('#'+menuDom).hide();

        }

        $scope.getPostSonNode = function (node,menuDom) {

            angular.element('#'+menuDom).show();

            //判断子分类是否已经存在缓存数据，存在则无需刷新
            if(isEmptySonData(node.id))
            {
                var  sonObj = { sonCate:[],sonHotKpi:[]};
                //获取子分类
                MenuService.post({cateId: node.id}, function (rs) {

                    sonObj.sonCate = angular.copy(rs.data);

                    setSonData(node.id,sonObj);

                }, function () {
                });

                //获取子分类热门指标
                HotKpiService.post({cateId: node.id, size:initHotKpiSize}, function (rs) {
                    sonObj.sonHotKpi = angular.copy(rs.data);
                    //设置子分类数据
                    setSonData(node.id,sonObj);

                }, function () {
                });
            }

        };

        initMenuData();     //初始化数据






}]); //开发范例



});