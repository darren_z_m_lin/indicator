define(["controllers/controllers","underscore", "services/services"], function (controllers,underscore) {

    controllers.controller("TagController", ["$scope", "tagManagementService","$location","SearchSharedService", function ($scope, TagManagementService,$location,sharedService) {
        console.log("TagController is call");
        /**
         *TagController
         *标管理签
         * -------------History------------------
         * DATE        AUTHOR    VERSION        DESCRIPTION
         * 2013-11-19   蔡书伟   V13.00.001      新增
         *
         *
         * author 蔡书伟
         */
        $scope.managerMode = 0;// 非管理
        $scope.newTags  = [];//添加新分类
        $scope.delTags  = [];//添加新分类
        var Tags = function()  //标签对象
        {
              return {
                "id":"",
                "isEdit":0,
                "editType":0,
                "isMark":0,
                "name":"",
                "selectCss":"filters",
                "index":0,
                "sonTags":[],
                "addTags":[]
             };
        };
        var postSTagObj = function()
        {
            return {
                "id":"",
                "name":""
            };
        }
        var postFTagObj = function()
        {
            return {
                "id":"",
                "name":"",
                "sonList":[]
            };
        }
        var initData = function(data)    //初始化数据
        {
            var tmpTableTags = [];   //父标签数组
            var seleced = angular.copy(data.selectTags);
            angular.forEach(data.data,function(dataobj,index)    //迭代父标签
            {
                var tag = new Tags();
                tag.id = dataobj.id;
                tag.name =dataobj.name;
                tag.index = index;
                var tmpSonTableTags = [];   //子标签数组
                angular.forEach(dataobj.sonList,function(tagObj,index2)    //迭代子标签
                {
                    var sontag = new Tags();
                    sontag.id = tagObj.id;
                    sontag.name =tagObj.name;
                    sontag.index = index2;
                    if(underscore.indexOf(seleced, tagObj.id)!=-1)//找不到已选
                    {
                        sontag.selectCss = "filters selected";
                        sontag.isMark=1;
                    }
                    this.push(sontag);
                },tmpSonTableTags);
                tag.sonTags =  tmpSonTableTags;
                this.push(tag);
            },tmpTableTags);

           return    tmpTableTags;
        };
        $scope.mark = function(tagIndex,sonTagObj)  //打标签
        {
            if($scope.managerMode !=1)
            {
                var sonTags = $scope.tags[tagIndex];
                angular.forEach(sonTags.sonTags,function(tagObj,index)    //迭代子标签
                {
                    if(tagObj.id ==  sonTagObj.id)
                    {
                        if(tagObj.selectCss=="filters")
                        {
                            tagObj.selectCss="filters selected";
                            tagObj.isMark=1;
                        }
                        else
                        {
                            tagObj.selectCss="filters"
                            tagObj.isMark=0;
                        }
                        return;
                    }
                });
            }
        };
        $scope.addNewFatherTag = function() //添加新父标签
        {
            var newTag = new Tags();
            newTag.isEdit=1;
            newTag.editType =1;
            newTag.index=$scope.newTags.length;
            var newSonTag = new Tags();
            newSonTag.isEdit = 1;
            newSonTag.editType =1;
            newSonTag.selectCss = "filters edit_state";
            newSonTag.index=newTag.addTags.length;
            newTag.addTags.push(newSonTag);
            $scope.newTags.push(newTag);
        };
        $scope.addSonTag = function(tagIndex,type) //添加子标签
        {
            var sonTags = null;
            if(type==1)//旧有分类添加子类
            {
                sonTags = $scope.tags[tagIndex];
            }
            else//新分类添加子类
            {
                sonTags = $scope.newTags[tagIndex];
            }
            var newtag = new Tags();
            newtag.isEdit=1;
            newtag.editType=1;
            newtag.index = sonTags.addTags.length;
            newtag.selectCss = "filters edit_state";
            sonTags.addTags.push(newtag);
        };
        $scope.editTag = function(sonTagObj) //编辑标签
        {
            sonTagObj.isEdit=1;
            sonTagObj.editType=1;
            sonTagObj.selectCss=sonTagObj.selectCss+" edit_state";
        };

        var cleanStatus = function(tags)    //结束管理清除标签状态
        {
            angular.forEach(tags,function(fTagbj,index)    //迭代父标签
            {
                fTagbj.isEdit=0
                fTagbj.editType=0;
                fTagbj.selectCss = fTagbj.selectCss.replace("edit_state","");
                fTagbj.addTags = [];
                angular.forEach(fTagbj.sonTags,function(tagObj,index2)    //迭代子标签
                {
                    tagObj.isEdit=0;
                    tagObj.editType=0;
                    tagObj.selectCss = tagObj.selectCss.replace("edit_state","");
                });
            });
        };

        $scope.manager = function(type,isGetData)
        {
            if(type=="open")
            {
                $scope.managerMode =1;
                angular.element(".filter_edit_container").addClass("filter_edit_mode");
                angular.element("#goto_manage_tag").hide();
                angular.element("#now_managing_tag").show();
            }
            else
            {
                $scope.managerMode =0;
                angular.element(".filter_edit_container").removeClass("filter_edit_mode");
                angular.element("#goto_manage_tag").show();
                angular.element("#now_managing_tag").hide();
                if(isGetData)
                {
                    $scope.saveAll(0); //完成管理后保存 ，不用清除，因为已经干净
                }
                else
                {
                    $scope.newTags  = [];//清空新分类
                    $scope.delTags  = [];//清空删除分类
                }

            }
        };
        var postDataObj = function()  //请求对象
        {
            return {
                "kpiId":"",
                "operateData":[],
                "selectTags":[],
                "delTags":[]
            };
        };
        var getPostData = function(type,postData)   //根据类型获取请求对象
        {
            var tmpPostDataObj = new postDataObj();
            tmpPostDataObj.kpiId =  $scope.kpiId;
            if(type=="saveAll")
            {
                tmpPostDataObj = postData;

            }
            else if (type == "delTag")
            {
                tmpPostDataObj.delTag = postData;
            }
            else
            {
                tmpPostDataObj.operateData = postData;
            }
             return     tmpPostDataObj;
        }
        $scope.saveTagEdit = function(sonTagObj)  //editTyp编辑类型,saveType保存类型1为已存在，2为新增的标签
        {
            sonTagObj.isEdit=0;
            sonTagObj.selectCss = sonTagObj.selectCss.replace("edit_state","");
        }
        $scope.delTag = function(TagsIndex,delIndex,TagObj,delType,ObjType)    //删除标签
        {
            var tmpTags = [];
            var tmpSonTags = [];

            if(delType=="old")
            {
                tmpTags = $scope.tags;
                if(ObjType==1)    //旧有父类
                {
                    tmpTags.splice(TagsIndex, 1);
                    $scope.delTags.push(TagObj.id);
                }
                else if(ObjType==2)  //旧有子类
                {
                    tmpSonTags =  tmpTags[TagsIndex];
                    tmpSonTags.sonTags.splice(delIndex, 1);
                    $scope.delTags.push(TagObj.id);
                }
                else            //新增子类
                {
                    tmpSonTags =  tmpTags[TagsIndex];
                    tmpSonTags.addTags.splice(delIndex, 1);
                }
            }
            else
            {
                tmpTags = $scope.newTags;
                if(ObjType==1) //新增父类
                {
                    tmpTags.splice(TagsIndex, 1);
                }
                else     //新增子类
                {
                    tmpSonTags =  tmpTags[TagsIndex];
                    tmpSonTags.addTags.splice(delIndex, 1);
                }
            }
        }
        $scope.saveAll = function(cleanType)    //全部保存
        {
            var tmpTags  =   angular.copy($scope.tags);
            var tmpNewTags  =   angular.copy($scope.newTags);
            var tmpPostDataObj = new postDataObj();
            tmpPostDataObj.kpiId =  $scope.kpiId;
            var selectTags = [];
            //迭代已有修改的标签
            angular.forEach(tmpTags,function(fTagbj,index)    //修改迭代父标签
            {
               var checkIsEdit = false;
               var checkEmpty = false;
               var tmpPostFTagObj  = new postFTagObj();//父类对象
                angular.forEach(fTagbj.sonTags,function(tagObj,index2)    //迭代子标签
                {
                    if(tagObj.editType==1)
                    {
                        var tmpPostSTagObj  = new postSTagObj();//子类对象
                        if(!underscore.isEmpty(tagObj.name))
                        {
                            tmpPostSTagObj.id = tagObj.id;
                            tmpPostSTagObj.name = tagObj.name;
                            tmpPostFTagObj.sonList.push(tmpPostSTagObj);
                        }
                        else
                        {
                            checkEmpty = true;
                        }
                        checkIsEdit  = true;
                    }
                    if(tagObj.isMark==1)
                    {
                        selectTags.push(tagObj.id);
                    }
                });
                angular.forEach(fTagbj.addTags,function(tagObj,index2)    //迭代子标签
                {
                        var tmpPostSTagObj  = new postSTagObj();//子类对象
                        if(!underscore.isEmpty(tagObj.name))
                        {
                            tmpPostSTagObj.id = tagObj.id;
                            tmpPostSTagObj.name = tagObj.name;
                            tmpPostFTagObj.sonList.push(tmpPostSTagObj);
                        }
                        else
                        {
                            checkEmpty = true;
                        }
                        checkIsEdit  = true;
                });
                if(fTagbj.editType==1||checkIsEdit)//检查是否存在编辑以及新增的标签
                {
                    if(!underscore.isEmpty(fTagbj.name))
                    {
                        tmpPostFTagObj.id = fTagbj.id;
                        tmpPostFTagObj.name = fTagbj.name;
                    }
                    else
                    {
                        checkEmpty = true;
                    }
                    checkIsEdit  = true;
                }
                if(checkIsEdit)
                {
                    tmpPostDataObj.operateData.push(tmpPostFTagObj);
                }

            });
            tmpPostDataObj.selectTags =  selectTags;  //打标签选项
            tmpPostDataObj.delTags = $scope.delTags;    //删除的
            //迭代新添加的标签
            angular.forEach(tmpNewTags,function(fNewTagbj,index)    //修改迭代父标签
            {
                var checkIsAdd = false;
                var tmpPostFTagObj  = new postFTagObj();//父类对象
                angular.forEach(fNewTagbj.addTags,function(tagNewObj,index2)    //迭代子标签
                {
                        var tmpPostSTagObj  = new postSTagObj();//子类对象
                        if(!underscore.isEmpty(tagNewObj.name))
                        {
                            tmpPostSTagObj.id = tagNewObj.id;
                            tmpPostSTagObj.name = tagNewObj.name;
                            tmpPostFTagObj.sonList.push(tmpPostSTagObj);
                        }
                        else
                        {
                            checkEmpty = true;
                        }
                });
                if(!underscore.isEmpty(fNewTagbj.name))
                {
                    tmpPostFTagObj.id = fNewTagbj.id;
                    tmpPostFTagObj.name = fNewTagbj.name;
                    checkIsAdd = true;
                }
                else
                {
                    checkEmpty = true;
                }
                if(checkIsAdd) //新增父类不为空时候才添加子类
                {
                    tmpPostDataObj.operateData.push(tmpPostFTagObj);
                }
            });
            var params = getPostData("saveAll",tmpPostDataObj);
            TagManagementService.get(params, function (rs){
                $scope.tags =  initData(rs);
                $scope.managerMode = 0;// 非管理
                $scope.newTags  = [];//清空新分类
                $scope.delTags  = [];//清空删除分类
                if(cleanType==1)    //是否清除管理状态
                {
                    $scope.manager("close",false);
                }

            });
        }
        $scope.$on("handleBroadcast", function () {
            if (sharedService.message.event === "IndicatorManagerCtrl:tagsEdit") {
               // console.log("IndicatorManagerCtrl:tagsEdit even");
                $scope.kpiId =   sharedService.message.kpiId;
                var params = getPostData("getData",[]);
                TagManagementService.get(params, function (rs){
                    $scope.tags =  initData(rs);
                    $scope.managerMode = 0;// 非管理
                    $scope.newTags  = [];//添加新分类
                    $scope.manager("close",false);
                });
            }
        });//处理广播事件
}]); //开发范例

});