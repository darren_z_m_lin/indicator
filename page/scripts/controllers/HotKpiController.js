define(["controllers/controllers", "services/services"], function (controllers) {

    controllers.controller("HotKpiController", ["$scope", "HotKpiService","cache","$location", function ($scope, HotKpiService,kpiCache,$location) {
        //console.log("HotKpiController is call");
        /**
         *热门指标HotKpiController
         *
         *
         * -------------History------------------
         * DATE        AUTHOR    VERSION        DESCRIPTION
         * 2013-09-16   蔡书伟   V13.00.001      新增
         *
         *
         * author 蔡书伟
         */
        var menuCacheName ="menuData";
        //初始化数组对象
        var initData = [];
        //初始化cateid属性值
        var initCateId ="9999";
        //初始化获取热门指标数
        var initHotKpiSize = 10;

        //更新model
        var changeModel = function(responseData)
        {
            $scope.indexHotKpi = responseData;
        }
        //设置css上升或下降属性类
        var setclass = function(drift)
        {
            if(drift===1)
             {
                 return "num_up";
             }else
             {
                 return "num_down";
             }
        }
        //热门指标属性
        var hotKpi = function (item) {
            return {
                "id": item.id,
                "name":item.name,
                "value":item.value,
                "class":setclass(item.drift)
            };
        };
        //初始化父分类数据
        var setInitData = function (responseData) {
            angular.forEach(responseData, function (item, index) {
                var hotKpitem = new hotKpi(item);
                this.push(hotKpitem);
            },initData);

            return initData;
        }
        var initIndexHotKpi = function ()
        {

            HotKpiService.post({cateId:initCateId, size:initHotKpiSize}, function (rs) {
               changeModel(setInitData(rs.data));
            }, function () {
            });
        }


        $scope.detail = function(kpiobj) {
            $location.path("detail").search({kpiId:kpiobj.id});
        }








        initIndexHotKpi();     //初始化数据
//
//        $scope.cfg = {
//        classes:{},
//        actions:{
//            add:function(){
//            }
//          },
//        data:[]
//       };





}]); //开发范例

    controllers.controller("MenuKpiHotController", ["$scope", "Category", function ($scope, category) {
        //console.log("MenuKpiHotController is call");

    }]); //开发范例

});