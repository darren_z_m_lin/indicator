﻿/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["controllers/controllers", "underscore", "services/services"], function (controllers, _) {
    /**
     * 指标管理器控制器 创建人 -林志敏
     * 2013-11-18
     */
    controllers.controller("IndicatorManagerCtrl", [
        "$scope",
        "$log",
        "$location",
        "KpiManagementService",
        "KpiInfoService",
        "DimensionService",
        "RootCategoriesService",
        function($scope, $log, $location, KpiManagementService, KpiInfoService, DimensionService, RootCategoriesService) {
            var hash = $location.hash();
            var context = this;
            $scope.cateId = '1';
            //todo 获取分类根节点列表 更新options
            $scope.options = {1:"全部",2:"网络",3:"财务",4:"用户",5:"产品",6:"渠道",7:"资源"};
            $scope.newKpi = {};//新添指标页面的域对象
            var selectedKpis = [];//储存选择的指标集合
            $scope.tmpKpi = {};//编辑指标页面的域对象
            $scope.remove = function() {

            };//删除按钮的动作
            $scope.add = function() {
                $scope.newKpi = {};
                //调用添加接口
                $log.info("add:" + $scope.newKpi);
            };//打开添加指标的动作
            $scope.save = function() {
                //调用修改接口
                $log.info($scope.tmpKpi);
            };//保存添加页面的数据或者修改页面的数据
            var params = {
                pageNumber:1,
                pageSize:10,
                kpiName:"",
                kpiNo:"",
                cateId:""
            };//请求参数对象
            var searchKpi = function(params, $scope, selectedKpis) {
                var response = {};
                //todo 从远程服务器获取KPI 并更新resopnse变量
                response = {
                    request:{
                        "pageNumber":1,
                        "pageSize":10,
                        "kpiName":"指标名称",
                        "kpiNO":"指标编码",
                        "cateId":"分类Id"
                    },
                    data:{

                        total:10,

                        kpis:[
                            {
                                "kpiId":"1",//包含指标id为更新，不包含为新增
                                "kpiName":"指标名称",
                                "kpiNo":"指标编码",
                                "unitName":"计量单位",
                                "tableName":"对应模型表名称",
                                "columnENname":"对应指标字段英文名",
                                "cateRootName":"网络",
                                "dimension":{id:"指标维度信息"},
                                "inspectFrequency":{id:"考察频度"},
                                "serviceDesc":"业务口径"

                                /**
                                 "colummCHname":"对应指标字段中文名",
                                 "modelisvalid":"模型是否上线",
                                 "kpiissvalid":"指标是否生效",
                                 "validdate":"有效开始时间",
                                 "invaliddate":"有效结束时间",
                                 "modelName":"对应模型中文名称",
                                 "objectName":"对象"
                                 */
                            }
                        ]}
                };//假数据
                //todo 获取指标列表数据
                //todo 根据被选列表 更新获取数据的被选状态
                var addBehaviour = function(kpis, $scope, selectedKpis) {
                    angular.forEach(kpis, function(obj) {
                        obj.edit = angular.element.proxy(function(obj) {
                            $scope.tmpKpi = angular.copy(obj);
                            $log.info($scope.tmpKpi);
                        }, context, obj);
                        obj.toggle = angular.element.proxy(function(obj) {
                            var target;
                            angular.forEach(selectedKpis, function(item, index) {
                                if (obj.kpiId === item.kpiId)
                                    target = {index:index,item:item};
                            });
                            if (angular.isUndefined(target)) {
                                selectedKpis.push(obj);//添加选择
                                return;
                            }
                            selectedKpis.splice(target.index, 1);//删除选择
                        }, context, obj);
                        obj.tag = angular.element.proxy(function(obj) {
                            $log.info(obj);
                        }, context, obj);
                    });
                };
                addBehaviour(response.data.kpis, $scope, selectedKpis);//添加指标的行为
                $scope.response = response;//刷新指标页面指标列表
            };//用于刷新指标管理页面的列表
            searchKpi(params, $scope, selectedKpis);
            $scope.search = function(params) {
                $log.info("indicatorName:" + params.indicatorName + " indicatorCode:" + params.indicatorCode + " cateId:" + params.cateId);
            };//查询按钮的动作
            $scope.pageClick = {method:function(page) {
                $log.info(page);//修改页数 获取指标列表数据 刷新指标列表
            },context:context};//分页控件的动作
        }]);
    /**
     * 分类管理页面控制器 创建人:林志敏，蔡书伟
     */
    controllers.controller("CategoriesCtrl", ["$scope", "categories", "TreeService", "$timeout", "expand", "Category","$log","SortedKpisService","UpdateCateService",
        function factory($scope, categories, treeService, $timeout, expand, Category, $log, sortedKpisService, updateCateService) {
            //console.log(categories);
            var selectionManager = (function(angular) {
                var selection = [];
                var set = function(selection) {
                    this.selection = selection;
                };
                var isExist = function(kpi) {
                    var isExist = false;
                    var loop = true;

                    angular.forEach(this.selection, function(item) {
                        if (loop) {
                            if (item.id === kpi.id) {
                                isExist = true;
                                loop = false;
                            }
                        }
                    });
                    return isExist;
                };
                var add = function(kpi) {
                    if (!isExist(kpi)) {
                        this.selection.push(kpi);
                    }
                };
                var remove = function(kpi) {
                    angular.forEach(this.selection, function(item, index) {
                        if (item.id === kpi.id)
                            this.splice(index, 1);
                    }, this.selection);
                };
                var get = function() {
                    return this.selection;
                };
                return {add:add,remove:remove,get:get,set:set,isExist:isExist};
            })(angular);
            $scope.tree = treeService.getTree();//指标分类树
            var orgToTgt = function (org) {
                var code;
                var key;
                var unselected;
                var selected;
                var other;
                var start;
                var end;
                var tmp;
                var comKey;
                var target = {
                    unselected:{},selected:[]
                };
                for (code = 65; code < 91; code++) {
                    key = String.fromCharCode(code);
                    if (org.selectedMap[key]) target.selected = target.selected.concat(org.selectedMap[key]);
                    if (code % 4 === 1) {
                        unselected = [];
                        selected = [];
                        other = [];
                        start = key;
                        if (org.unselectedMap[key]) unselected = unselected.concat(org.unselectedMap[key]);
                        if (org.otherSelectedMap[key]) other = other.concat(org.otherSelectedMap[key]);
                    } else if (code % 4 === 0 || code === 90) {
                        end = key;
                        if (org.unselectedMap[key]) unselected = unselected.concat(org.unselectedMap[key]);
                        if (org.otherSelectedMap[key]) other = other.concat(org.otherSelectedMap[key]);
                        comKey = start + "_" + end;
                        target.unselected[comKey] = {};
                        target.unselected[comKey].unselected = unselected;
                        target.unselected[comKey].other = other;
                    } else {
                        if (org.unselectedMap[key]) unselected = unselected.concat(org.unselectedMap[key]);
                        if (org.otherSelectedMap[key]) other = other.concat(org.otherSelectedMap[key]);
                    }
                }
                return target;
            };//把服务端数据结构转换成页面显示数据结构 只包含没有被用户选择的数据
            var orgToArray = function (org) {
                var tmp = [];
                angular.forEach(org.unselected, function(indexEntity, index) {
                    angular.forEach(indexEntity, function(array, type) {
                        tmp = tmp.concat(array);
                    });
                });
                return tmp;
            };//把服务器数据结构转换成数组 只包含没有被用户选择的数据
            var all;//储存初始化数据
            var initKpiSelector = function() {
                angular.element(".item_sort_box.unsorted .sorted_other").tooltip();
                angular.element(".nav-tabs a:first").tab('show');
                angular.element(".index_query").each(function() {
                    angular.element(this).find("a:first").tab('show');
                });
            };//初始化指标选择器
            var filter = function (selected, targets, all) {
                var tmp = {
                    selected: selected,
                    unselected: {

                    }
                };
                angular.forEach(all.unselected, function (indexEntity, index) {
                    tmp.unselected[index] = {
                        unselected: _.filter(indexEntity.unselected, function (unselected) {
                            var exist = false;
                            angular.forEach(targets, function (target) {
                                if (target.id === unselected.id) exist = true;
                            });
                            return exist;
                        }),
                        other: _.filter(indexEntity.other, function (other) {
                            var exist = false;
                            angular.forEach(targets, function (target) {
                                if (target.id === other.id) exist = true;
                            });
                            return exist;
                        })
                    };
                });
                return tmp;
            };//获取显示的数据
            $scope.kpis = {
                unselected:{},selected:[]
            };//显示数据的格式
            $scope.params = {
                keyword:"",
                cateId:""
            };//请求参数
            $scope.query = function() {
                sortedKpisService.get({keyword:$scope.params.keyword,cateId:$scope.params.cateId}, function(data) {
                    angular.forEach(data.unselectedMap, function(array) {
                        angular.forEach(array, function(kpi) {
                            kpi['selected'] = false;
                        });
                    });//添加分类选择状态属性
                    angular.forEach(data.otherSelectedMap, function(array) {
                        angular.forEach(array, function(kpi) {
                            kpi['selected'] = false;
                        });
                    });//添加分类选择状态属性
                    var tmp = orgToTgt(data);//后台数据转换成前台数据显示的格式
                    $scope.kpis = filter(selectionManager.get(), orgToArray(tmp), all);//更新前台页面数据
                });
            };//根据关键字和分类从服务器获取指标选择器没有被选择的指标集合 并更新该集合的页面显示
            $scope.all = function() {
                sortedKpisService.get({keyword:$scope.params.keyword,cateId:$scope.params.cateId}, function(data) {
                    angular.forEach(data.unselectedMap, function(array) {
                        angular.forEach(array, function(kpi) {
                            kpi['selected'] = false;
                        });
                    });//添加显示数据样式属性
                    angular.forEach(data.otherSelectedMap, function(array) {
                        angular.forEach(array, function(kpi) {
                            kpi['selected'] = false;
                        });
                    });//添加显示数据样式属性
                    var tmp = orgToTgt(data);//格式化服务器数据为显示数据格式
                    all = tmp;//缓存指标全部分类
                    $scope.kpis = tmp;//刷新页面
                    selectionManager.set(tmp.selected);//初始化已选项
                    $timeout(initKpiSelector);//初始化指标选择器
                });
            };//获取指标全部分类
            var init = function () {

                angular.element(".item_sort_box.unsorted .sorted_other").tooltip();
                //判断是否有下级，如果有，自动添加展开的按钮
                angular.element("#manage_tree_ul .tree_item").each(function() {
                    var data = angular.element(this).attr("data");
                    if (angular.element(this).find(">.manage_tree_ul").size() > 0) {
                        angular.element(this).find(".tree_item_box:first").append("<div class='expand_trigger' data='" + data + "'></div>");
                    }
                })

                //移上item之后需要显示item名字，以及其事件处理
                /*angular.element("#manage_tree_ul .tree_item").not(".add_tree_root_item").find(".tree_item_box").append("<div class='item_title'>item的名字的名字的名字的名字</div>");*/
                //把移上树节点时所出现的按钮多加个判断，是为了把指标移动到节点的时候不会出现按钮
                var indicator_move = 0;//增加一个判断鼠标是否从指标移动过来的全局变量
                angular.element("#manage_tree_ul .items").hover(
                    function () {
                        if (indicator_move == 0) {
                            angular.element(this).siblings(".item_title").show().end().find(".item_wrapper").addClass("hover");
                        }
                    },
                    function () {
                        if (indicator_move == 0) {
                            angular.element(this).siblings(".item_title").hide().end().find(".item_wrapper").removeClass("hover");
                        }
                    }
                );

                //下列是初始化用的动作
                angular.element("#manage_tree_ul .tree_item").addClass("clearfix");//对所"tree_item"添加"clearfix"的class用于清除浮动
                //对"tree_item"赋予所属层级以标记颜色
                angular.element("#manage_tree_ul>.tree_item").addClass("item_lv1");
                angular.element("#manage_tree_ul>.tree_item>.manage_tree_ul>.tree_item").addClass("item_lv2");
                angular.element("#manage_tree_ul>.tree_item>.manage_tree_ul>.tree_item .tree_item").addClass("item_lv3");
                //08-20新增，因为要做到向左排列不换行，必须改变部分CSS代码，导致不能用CSS隐藏此元素，改为用下面这行JS来隐藏
                angular.element("#manage_tree_ul .manage_tree_ul").hide();
                //08-20新增，初始化时判断树是否能左右滚动，执行此函数的相应动作
                ManageTreeScrollShow();
                //IE6去死
                if (angular.element.browser.msie && angular.element.browser.version == 6.0) {

                    //鼠标移高亮节点功能
                    angular.element("#manage_tree_ul .func_btn").hover(
                        function () {
                            angular.element(this).css("backgroundColor", "#333");
                        },
                        function () {
                            angular.element(this).css("backgroundColor", "transparent");
                        }
                    );
                    //修改每棵树第一个节点的样式（IE6以上支持直接CSS选择第一个元素）
//		angular.element("#manage_tree_ul").find(".manage_tree_ul").each(function(){
//			angular.element(this).find(".tree_item:first").find(">.tree_item_box").addClass("fir");
//		})

                }

                //修改每棵树最后一个节点的样式（IE9以上才支持直接CSS选择最后一个元素）
//	if (angular.element.browser.msie && angular.element.browser.version<=8.0) {
//		angular.element("#manage_tree_ul").find(".manage_tree_ul").each(function(){
//			angular.element(this).find(">.tree_item:last").find(".tree_item_box").addClass("last");
//		})
//	}

                //以下为08-20新增，树的左右滚动相关脚本
                angular.element(".manage_tree_scroll.left").click(function() {//点击左滚动按钮，向左滚310像素（每个item是155像素，310像素是2个item的宽度）
                    ManageTreeScroll(-310);
                });
                angular.element(".manage_tree_scroll.right").click(function() {//点击右滚动按钮，向右滚310像素
                    ManageTreeScroll(310);
                });
                //树的水平滚动函数
                function ManageTreeScroll(x) {
                    angular.element("#manage_tree_ul").animate({ scrollLeft: angular.element("#manage_tree_ul").scrollLeft() + x }, 400);
                }

                //判断el是否可水平滚动，返回逻辑，任何改变宽度的情况，都可以按需要使用。可通用到其他地方
                function isScrollX(el) {
                    var scrollX = false;
                    // test horizontal
                    var sl = el.scrollLeft();
                    el.scrollLeft(sl + (sl > 0) ? -1 : 1);
                    el.scrollLeft() !== sl && (scrollX = scrollX || true);
                    el.scrollLeft(sl);
                    return scrollX;
                }


                //控制左右滚动按钮出现与否
                function ManageTreeScrollShow() {
                    var el = angular.element(".manage_tree_scroll");
                    if (isScrollX(angular.element("#manage_tree_ul"))) {
                        el.show();
                        ManageTreeScroll(310);
                    } else {
                        el.hide();
                    }
                }

                //鼠标拖动树移动
                var _move;
                angular.element("#manage_tree_ul").mousedown(function(e) {
                    if (isScrollX(angular.element("#manage_tree_ul"))) {
                        _move = true;
                        _x = e.pageX + parseInt(angular.element("#manage_tree_ul").scrollLeft());
                    }
                });
                angular.element(document).mousemove(
                    function(e) {
                        if (_move) {
                            var x = _x - e.pageX;
                            angular.element("#manage_tree_ul").scrollLeft(x).css("cursor", "move");
                        }
                    }).mouseup(function() {
                        _move = false;
                        angular.element("#manage_tree_ul").css("cursor", "default");
                    });
                //让左右滚动按钮在固定位置
                var manage_tree_scroll_offset = parseInt(angular.element(".manage_tree_scroll").css("top"));
                angular.element(window).scroll(function() {
                    if (isScrollX(angular.element("#manage_tree_ul"))) {
                        angular.element(".manage_tree_scroll").css("top", manage_tree_scroll_offset + angular.element(window).scrollTop());
                    }
                });


                //指标归类的容器

                //容器折叠
                angular.element(".float_config_header").click(function() {
                    var _ = angular.element(this);
                    angular.element(this).next().slideToggle("fast", function() {
                        _.toggleClass("folded");
                    });
                });

                //指标选择高亮
                angular.element(".indicator_config_list li").not(".indicator_config_sort").click(function() {
                    angular.element(this).toggleClass("selected");
                });

                //切换"仅显示未分类"
                angular.element("#only_not_sort_check").click(function() {
                    angular.element(".indicator_config_list").toggleClass("hide_sort");
                    if (angular.element(this).attr("checked")) {//预防把"未分类"给折叠了之后勾选"仅显示未分类"
                        angular.element("#unsort_list").show();
                    }
                });

                //指标分类折叠
                angular.element(".indicator_config_sort").click(function() {
                    angular.element(this).next().slideToggle("fast");
                });

                /*//拖动指标时要触发的
                 var _idmove,timer,testdrag = 0;//_idmove用于与上面的_move分开，不会产生树的左右拖动个指标的拖动判断混乱；timer用于拖动开始后多少秒的计时器；testdrag用于判断mousemove的时候只运行一次对应脚本
                 angular.element(".indicator_config_list li").mousedown(function() {
                 _idmove = true;
                 });
                 angular.element(document).mousemove(
                 function() {
                 if (_idmove && testdrag == 0) {
                 timer = setTimeout(function() {
                 angular.element("#float_config_content").slideUp("fast", function() {
                 angular.element("#float_config_header").addClass("folded");
                 });
                 indicator_move = 1; //这里的indicator_move 是34行时定义的变量，用于判断是否从指标库拖动到树。这里既然产生了拖动，于是这个值变为1，代表是从指标库拖动到树这个条件成立
                 }, 500)
                 testdrag = testdrag + 1;
                 }
                 }).mouseup(function() {
                 _idmove = false;
                 testdrag = 0;
                 if (indicator_move == 0) {
                 clearInterval(timer);
                 } else {
                 indicator_move = 0;
                 angular.element("#float_config_content").slideDown("fast", function() {
                 angular.element("#float_config_header").removeClass("folded");
                 });
                 }
                 });*/
                //展开按钮的动作
                angular.element(".expand_trigger").click({expand: expand}, function (e) {
                    angular.element(this).parent(".tree_item_box").next(".manage_tree_ul").slideToggle("fast", function() {
                        ManageTreeScrollShow();
                    });//08-20修改
                    angular.element(this).toggleClass("trigger_opened");
                    var id = angular.element(this).attr("data");
                    if (angular.element(this).hasClass("trigger_opened")) {
                        e.data.expand.addExpand(id);
                    } else {
                        e.data.expand.removeExpand(id);
                    }
                });
                //刷新树之后还原状态
                var ids = expand.getExpand();//获取上次打开的树节点id
                var select = [];
                var string = "";
                angular.forEach(ids, function (id, index) {
                    this.push(".expand_trigger[data=" + id + "]");
                }, select);
                string = select.join(",");//根据获取的上次打开的树节点id集合用jquery选择器找出相关树节点展开器
                angular.element(string).toggleClass("trigger_opened");//打开展开器
                angular.element(string).each(function () {
                    angular.element(this).parent(".tree_item_box").next(".manage_tree_ul").slideToggle("fast");
                });

                //IE6去死
                if (angular.element.browser.msie && angular.element.browser.version == 6.0) {

                    //重要！鼠标移上出现节点功能
                    angular.element(".item_wrapper").hover(
                        function () {
                            angular.element(this).css("marginLeft", "0");
                        },
                        function () {
                            angular.element(this).css("marginLeft", "-100px");
                        }
                    );
                    //鼠标移高亮节点功能
                    angular.element(".func_btn").hover(
                        function () {
                            angular.element(this).css("backgroundColor", "#333");
                        },
                        function () {
                            angular.element(this).css("backgroundColor", "transparent");
                        }
                    );
                    //修改每棵树第一个节点的样式（IE6以上支持直接CSS选择第一个元素）
                    angular.element("#manage_tree_ul").find(".manage_tree_ul").each(function () {
                        angular.element(this).find(".tree_item:first").find(">.tree_item_box").addClass("fir");
                    })

                }
                //修改每棵树最后一个节点的样式（IE9以上才支持直接CSS选择最后一个元素）
                if (angular.element.browser.msie && angular.element.browser.version <= 8.0) {
                    angular.element("#manage_tree_ul").find(".manage_tree_ul").each(function () {
                        angular.element(this).find(">.tree_item:last").find(".tree_item_box").addClass("last");
                    })
                }


            };
            var search = function(kpi, kpis) {
                var target;
                var loop = true;
                angular.forEach(kpis.unselected, function(v) {
                    if (loop) {
                        angular.forEach(v.unselected, function(item) {
                            if (loop) {
                                if (item.id === kpi.id) {
                                    target = item;
                                    loop = false;
                                }
                            }
                        });
                        angular.forEach(v.other, function(item) {
                            if (loop) {
                                if (item.id === kpi.id) {
                                    target = item;
                                    loop = false;
                                }
                            }
                        });
                    }
                });
                return target;
            };//根据索引和指标id查找指标
            $scope.toggleKpi = function(kpi) {
                var target = search(kpi, $scope.kpis);
                if (selectionManager.isExist(target)) {
                    selectionManager.remove(target);
                    // $scope.$apply(function(){
                    target.selected = false;
                    // });
                } else {
                    selectionManager.add(target);
                    // $scope.$apply(function(){
                    target.selected = true;
                    //});
                }
            };//全部指标列表中的点击事件
            $scope.edit = function (nodedata) {
                var data = angular.copy(nodedata);
                $scope.params.keyword = "";
                $scope.params.cateId = data.cateId;
                angular.element("#myModalLabel").text("编辑『分类』");
                $scope.formItem = angular.copy(nodedata);
                $scope.formItem.actionType = "edit";
                //Category.save({obj:data});
                //根据分类id获取该分类的指标集合
                //获取全部指标
                //console.info(nodedata);
                $scope.all();
            };//打开编辑窗口
            $scope.add = function (nodedata) {
                var initdata = {parentID: null};
                $scope.formItem = angular.copy(initdata);
                $scope.formItem.actionType = "add";
                if (nodedata != null) {
                    var data = angular.copy(nodedata);
                    $scope.formItem.parentID = data.cateId;
                }
            };//打开添加窗口
            $scope.del = function (nodedata) {
                var data = angular.copy(nodedata);
                if (confirm("确认删除[" + data.cateName + "]分类么？")) {
                    Category.delete({cateId: data.cateId}, function () {
                        $scope.tree = treeService.removeById(data.cateId);
                        $timeout(init);
                    }, function () {
                        alert("删除失败");
                    });
                }

            };//删除节点
            $scope.save = function (nodedata) {
                var treeobj = [];
                var setdata;
                var data = angular.copy(nodedata);
                if (data.actionType === "add") {
                    if (angular.isUndefined(data.parentID) || _.isNull(data.parentID))//判断是否一级树节点
                        data.parentID = "9999";
                    setdata = {cateName: data.cateName, cateNO: data.cateNO, parentID: data.parentID}
                    Category.save(setdata, function (rs) {
                        var treeNode = {
                            /*parent: category,*/
                            cateId: rs.cateId,
                            cateName: data.cateName,
                            cateNO: data.cateNO,
                            parentID: data.parentID,
                            //ord: category.ord,
                            isExpand: false,
                            children: []
                        };
                        treeobj = treeService.addTreeNode(treeNode);
                        angular.element('#pupup_box_add').modal('hide');
                        $scope.tree = treeobj;
                        $timeout(init);
                    }, function () {
                        alert("添加失败");
                    });
                }
                else {
                    var selection = selectionManager.get();
                    var ids = [];
                    angular.forEach(selection, function(val, key) {
                        ids.push(val.id);
                    });
                    setdata = {cateId: data.cateId, cateName: data.cateName, cateNo: data.cateNO, parentID: data.parentID, selected:ids};
                    //console.log(selectionManager.get());
                    updateCateService.update(setdata, function () {
                        var treeNode = {
                            cateId: data.cateId,
                            cateName: data.cateName,
                            cateNO: data.cateNO,
                            parentID: data.parentID
                        };
                        treeobj = treeService.updateTreeNode(treeNode);
                        console.log(treeobj);
                        angular.element('#pupup_box').modal('hide');
                        $scope.tree = treeobj;
                        $timeout(init);
                    }, function () {
                        alert("修改失败");
                    });
                }


            };//编辑或者添加窗口的确定按钮动作
            $timeout(init);//指标分类管理页面的初始化
        }]);//分类管理页面控制器
    /**
     * 主页控制器 创建人:林志敏
     */
    controllers.controller("RootCtrl", ["$scope", "$timeout", function ($scope, $timeout) {
        var init = function () {
            //视图切换
            angular.element(".title_func i").click(function () {
                angular.element(this).addClass("current").siblings("i").removeClass("current");
                if (angular.element(this).hasClass("icon-th-large") == 1) {
                    angular.element(".indicator_container").addClass("block_view");
                } else {
                    angular.element(".indicator_container").removeClass("block_view");
                }
            });
        };
        $timeout(init);
    }]);
    /**
     * 详细页面控制器 创建人:林志敏
     */
    controllers.controller("DetailCtrl", ["$scope","$timeout",function($scope, $timeout) {
        var init = function () {
            angular.element(function() {

                //给评价的显示脚本
                angular.element(".give_rate_star .icon-star").mouseover(function() {
                    var star = angular.element(this);
                    star.addClass("active").siblings(".icon-star").removeClass("active").siblings(".rate_score").find(".score").text($(this).index() + 1);
                    //angular.element(this).parent().find(".score_value").val($(this).index() + 1);
                    $scope.$apply(function() {
                        $scope.config.score = star.index() + 1;
                    });
                });

            });
        };
        $timeout(init); 
    }]);
    /**
     * 关键字控制器 创建人:林志敏
     */
    controllers.controller("KeywordController", [
        "$scope",
        "$log",
        "SearchService",
        "SearchSharedService",
        "$location",
        "$timeout",
        function ($scope, $log, searchService, sharedService, $location, $timeout) {
            $scope.keyword = "";
            var params = {
                search:{
                    tags:[],
                    cateId:"",
                    keyword:""
                },
                pager:{
                    pageNumber:1,
                    pageSize:10
                }
            };//默认页面初始化参数
            if (!angular.isUndefined($location.search()["keyword"])) {
                params.search.keyword = $location.search()["keyword"];
                $scope.keyword = $location.search()["keyword"];
            }
            if (!angular.isUndefined($location.search()["cateId"])) {
                params.search.cateId = $location.search()["cateId"];
            }
            if (!angular.isUndefined($location.search()["pageNumber"])) {
                params.pager.pageNumber = $location.search()["pageNumber"];
            }
            if (!angular.isUndefined($location.search()["tags"])) {
                params.search.tags = $location.search()["tags"];

            }
            $scope.$on("handleBroadcast", function() {
                var previous;
                if (sharedService.message.event === "MenuListController:Search") {
                    $location.path('index').search({cateId:sharedService.message.msg.id});
                } else if (sharedService.message.event === "FilterController:Changed") {
                    previous = $location.search();
                    previous['pageNumber'] = 1;
                    previous['tags'] = sharedService.message.msg.tags;
                    $location.search(previous);
                } else if (sharedService.message.event === "PagerController:Clicked") {
                    previous = $location.search();
                    previous['pageNumber'] = sharedService.message.msg.pageNumber;
                    $location.search(previous);
                }
            });
            $scope.search = function () {
                var tmp;
                if (angular.isUndefined($scope.keyword)) {
                    tmp = "";
                } else {
                    tmp = $scope.keyword;
                }
                $location.path("index").search({keyword:tmp});//刷新页面
            };//搜索按钮触发逻辑
            $scope.index = function() {
                $location.path('index').search({});
            };//返回到主页
            $timeout(function() {
                var result = searchService.search(
                    params, function () {
                        //broadcast msg to relative controllers
                        var message = {event:"KeywordController:Search",msg:{result:result}};
                        sharedService.prepForBroadcast(message);
                    });
            });//根据页面参数获取数据并显示数据
        }]);
    /**
     * 详细页面关键字控制器 创建人:林志敏
     */
    controllers.controller("DetailKeywordController", ['$scope','$log','$location','SearchService',
        function($scope, $log, $location, searchService) {
            var params = {
                search:{
                    tags:[],
                    cateId:"",
                    keyword:""
                },
                pager:{
                    pageNumber:1,
                    pageSize:10
                }
            };
            /*$scope.$watch("keyword", function(newVal) {
             if (newVal === undefined) {
             newVal = "";
             }
             params.search.keyword = newVal;
             }, true);*/
            $scope.search = function () {
                //broadcast msg to relative controllers
                $location.path("index").search({keyword:$scope.keyword});
            };//搜索按钮触发逻辑
            $scope.index = function() {
                $location.path('index').search({});
            };//返回到主页
        }]);
    /**
     * 过滤器控制器 创建人:林志敏
     */
    controllers.controller("FilterController", ["$scope", "$log", "SearchSharedService","$timeout", function ($scope, $log, sharedService, $timeout) {
        var tags;//过滤器控制器model
        var queryTag2dArray;
        var returnSelected = function() {
            var selected = [];
            angular.forEach(tags, function(tag) {
                var selectedSonList = [];
                angular.forEach(tag.sonList, function(son, key) {
                    if (son.selected === "selected" && son.id !== "") {
                        selectedSonList.push(son.id);
                    }
                });
                if (selectedSonList.length > 0)
                    selected.push(selectedSonList);

            });
            return selected;
        };//获取用户选择标签
        $scope.$on("handleBroadcast", function () {
            if (sharedService.message.event === "KeywordController:Search") {
                tags = sharedService.message.msg.result.tags;//从通讯服务中获取过滤器数据
                queryTag2dArray = sharedService.message.msg.result.params.search.tags;
                angular.forEach(tags, function(item) {
                    var tmp = [];
                    tmp.push({
                        selected:'selected',
                        name:'不限',
                        id:item.id + "0"
                    });
                    tmp = tmp.concat(item.sonList);
                    item.sonList = tmp; //添加不限选择项
                    angular.forEach(item.sonList, function(tag) {
                        //tag.selected = "";
                        tag.click = function() {
                            //双选模式 当用户点击不限的时候 清除相邻所有 当用户点击非不限选项时 清除不限选项
                            /*if (tag.id.length = 32) {
                             angular.forEach(item.sonList, function(son) {
                             son.selected = "";
                             });
                             } else {
                             item.sonList[0].selected = "";
                             }*/
                            //单选模式
                            angular.forEach(item.sonList, function(son) {
                                son.selected = "";
                            });//清除其他选项
                            tag.selected = "selected";
                            //$log.info(tags);
                            sharedService.prepForBroadcast({event:"FilterController:Changed",msg:{tags:returnSelected()}});//发已选过滤器选项到KeyrowrdController
                        };
                    });
                });//添加click事件到过滤器model
                angular.forEach(tags, function(item) {
                    angular.forEach(item.sonList, function(tag) {//循环每个显示标签的选项
                        angular.forEach(queryTag2dArray, function(queryTag1dArray) {//循环请求标签2维数组
                            angular.forEach(queryTag1dArray, function(queryTag) {//循环请求标签1维数组
                                if (queryTag === tag.id) {
                                    item.sonList[0].selected = "";//清除不限选项
                                    tag.selected = "selected";
                                }
                            });
                        });
                    });
                });//显示用户已选择的标签

                $scope.tags = tags;//更新过滤器的model

            }
        });//处理广播事件
    }]);
    /**
     * 指标列表控制器 创建人:林志敏
     */
    controllers.controller("KpisController", ["$scope", "$log", "SearchSharedService", function ($scope, $log, sharedService) {
        $scope.$on("handleBroadcast", function () {
            if (sharedService.message.event === "KeywordController:Search") {
                $scope.kpis = sharedService.message.msg.result.kpis;
            }
        });
    }]);
    /**
     * 分页控件控制器 创建人:林志敏
     */
    controllers.controller("PagerController", ["$scope", "$log", "SearchSharedService", function ($scope, $log, sharedService) {
        var context = this;

        $scope.$on("handleBroadcast", function () {
            if (sharedService.message.event === "KeywordController:Search") {
                var config = {};
                config.total = sharedService.message.msg.result.kpis.total;
                config.pageSize = 10;
                config.pageNumber = sharedService.message.msg.result.params.pager.pageNumber;
                config.click = {
                    context:context,
                    method:function(param) {
                        var message = {event:"PagerController:Clicked",msg:param};
                        sharedService.prepForBroadcast(message);
                    }
                };
                $scope.config = config;
            }
        });
    }]);
    /**
     * 指标显示器控制器 创建人:林志敏
     */
    controllers.controller("IndicatorController", [
        "$scope",
        "$log",
        "SearchSharedService",
        "AttentionService",
        "cache",
        "$location",
        function ($scope, $log, sharedService, attentionService, cache, $location) {
            //$log.info($scope.obj);
            var context = this;
            $scope.obj.click = function() {
                cache.put("kpiId", $scope.obj.id);
                $location.path("detail").search({kpiId:$scope.obj.id});
            };
            $scope.current = $scope.obj.values[6];
            $scope.obj.follow = function() {
                var careStatus;
                if ($scope.obj.careStatus === 1)
                    careStatus = 0;
                else
                    careStatus = 1;
                attentionService.update({
                    id:$scope.obj.id,
                    careStatus:careStatus
                }, function(result) {
                    if (angular.isUndefined(result.error))
                        $scope.obj.careStatus = careStatus;
                }, function() {

                });
            };
            $scope.obj.chartClick = {
                method:function(param) {
                    //$log.info(param);
                    $scope.$apply(function() {
                        $scope.current = $scope.obj.values[param.dataIndex];
                    });

                    $log.info($scope.current);
                },
                context:context
            };
        }]);
    /**
     * 搜索情景显示器控制器 创建人:林志敏
     */
    controllers.controller("CriteriaController", ["$scope","$log","SearchSharedService","$location",
        function($scope, $log, sharedService, $location) {
            $scope.$on("handleBroadcast", function() {
                if (sharedService.message.event === "KeywordController:Search") {

                    var tmp = sharedService.message.msg;
                    $scope.total = tmp.result.kpis.total;
                    $scope.keyword = tmp.result.params.search.keyword;
                    var params = sharedService.message.msg.result.params;
                    if ((params.search.keyword === "" || angular.isUndefined(params.search.keyword) ) &&
                        ((params.search.cateId === "" || angular.isUndefined(params.search.cateId))))
                        $scope.type = "我的关注";
                    else
                        $scope.type = "我的搜索";

                }
            });

        }]);
    /**
     * 基础信息控制器 创建人:林志敏
     */
    controllers.controller("BasicInfoController", [
        "$scope",
        "$log",
        "SearchSharedService",
        "BasicKpiInfoService",
        "cache",
        "$location",
        function($scope, $log, sharedService, basicInfoService, cache, $location) {
            /*$scope.$on("handleBroadcast", function() {
             if (sharedService.message.event === "KeywordController:Search") {

             }
             });*/
            var result = basicInfoService.get({
                //kpiId:cache.get("kpiId")
                kpiId:$location.search()['kpiId']
                //kpiId:"E3F94185A8E4195BE04014AC6120719A"
            }, function() {
                delete(result.$resolved);
                delete(result.$then);
                $scope.config = result;
            });
            //$scope.config = {key1:"i am key 1",key2:"i am key 2"};

        }]);
    /**
     * 评论控制器 创建人:林志敏
     */
    controllers.controller("CommentsController", [
        "$scope",
        "$log",
        "SearchSharedService",
        "KpiCommentService",
        "cache",
        "$location",
        function($scope, $log, sharedService, kpiCommentService, cache, $location) {
            /**
             * 当用户添加评论后刷新评论
             */
            $scope.$on("handleBroadcast", function() {
                if (sharedService.message.event === "AddCommentController:add") {
                    updateComments(1);
                }
            });
            var updateComments = function(pageNumber) {
                var comments = kpiCommentService.query({
                    //kpiId:"E3F94185A8E4195BE04014AC6120712A",
                    kpiId: $location.search()['kpiId'],
                    pager:{
                        pageNumber:pageNumber,
                        pageSize:10
                    }
                }, function() {
                    var context = this;
                    comments.pageSize = 10;
                    comments.click = {method:function(param) {
                        updateComments(param.pageNumber);
                    },
                        context:context};
                    $scope.config = comments;
                });
            };
            updateComments(1);

        }]);
    /**
     * 添加评论控制器 创建人:林志敏
     */
    controllers.controller("AddCommentController", [
        "$scope",
        "$log",
        "SearchSharedService",
        "AddKpiCommentService",
        "cache",
        "$location",
        function($scope, $log, sharedService, addCommentService, cache, $location) {
            /*$scope.$on("handleBroadcast", function() {
             if (sharedService.message.event === "KeywordController:Search") {

             }
             });*/
            $scope.$parent.config = {
                content:"",
                score:3,
                post:function() {
                    //$log.info($scope.$parent.config.content);
                    // $log.info($scope.$parent.config.score);
                    var result = addCommentService.put({
                        id: $location.search()['kpiId'],
                        //id:"E3F94185A8E4195BE04014AC6120712A",
                        review:$scope.$parent.config.content,
                        star:$scope.$parent.config.score
                    }, function() {
                        sharedService.prepForBroadcast({event:"AddCommentController:add"});
                    });

                }
            };

        }]);
    /**
     * 评论分页控制器 创建人:林志敏
     */
    controllers.controller("CommentsPagerController", ["$scope","$log","SearchSharedService","KpiCommentService","cache",
        function($scope, $log, sharedService, kpiCommentService, cache) {


        }]);
    /**
     * 推荐信息控制器 创建人:林志敏
     */
    controllers.controller("RecommendsController", [
        "$scope",
        "$log",
        "SearchSharedService",
        "RelativeKpiService",
        "cache",
        "$location",
        function($scope, $log, sharedService, relativeKpiService, cache, $location) {
            var relative = relativeKpiService.query({
                kpiId:$location.search()['kpiId']
                //kpiId:"E3F94185A8E8195BE04014AC6120713A"
            }, function() {
                angular.forEach(relative, function(item, key) {
                    item["click"] = function() {
                        $location.path("detail").search({kpiId:item.id});
                    };
                });
                $scope.config = {data:relative};
            });

        }]);
    /**
     * 导航控制器 创建人:林志敏
     */
    controllers.controller("MainMenuController", ["$scope","$log","SearchSharedService","$timeout",'$location',
        function($scope, $log, sharedService, $timeout, $location) {
            var context = this;
            var click = function(item) {
                $log.info("click: " + item);
                switch (item) {
                    case "kpi":
                        $location.path('indicator_manager');
                        break;
                    case "category":
                        $location.path('cat_manager');
                        break;
                    case "favourite":
                        $location.path('index').search({});
                        break;
                }
            };
            var config = {
                click:{
                    method:click,
                    context:context
                },
                favourite:false
            };
            $scope.config = config;
            $scope.$on("handleBroadcast", function() {
                if (sharedService.message.event === "KeywordController:Search") {
                    var params = sharedService.message.msg.result.params;
                    if ((params.search.keyword === "" || angular.isUndefined(params.search.keyword) ) &&
                        ((params.search.cateId === "" || angular.isUndefined(params.search.cateId)))) {
                        config.favourite = true;
                        $scope.config = config;
                        //$scope.$digest();
                    } else {
                        config.favourite = false;
                        $scope.config = config;
                        //$scope.$digest();
                    }
                }
            });

        }]);
    /**
     * 路径显示器控制器 创建人:林志敏
     */
    controllers.controller("LocationController", [
        "$scope",
        "$log",
        "SearchSharedService",
        "$location",
        function($scope, $log, sharedService, $location) {
            $scope.$on("handleBroadcast", function() {
                if (sharedService.message.event === "LargeIndicatorController:Refresh") {
                    var context = this;
                    var method = function(item) {
                        $location.path("index").search({cateId:item.id});
                    };
                    var categories = [];
                    angular.forEach(sharedService.message.msg.categories, function(item) {
                        categories.push({name:item.cateName,id:item.id});
                    });
                    var detail = sharedService.message.msg;
                    categories.push({name:detail.name,id:detail.id});
                    $scope.config = {
                        click:{
                            method:method,
                            context:context
                        },
                        links:categories};
                }
            });
        }]);
    /**
     * 指标大柱状图控制器 创建人:林志敏
     */
    controllers.controller("LargeChartController", ["$scope","$log","SearchSharedService","RelativeKpiService","$location",
        function($scope, $log, sharedService, relativeKpiService, $location) {
            $scope.$on("handleBroadcast", function() {
                if (sharedService.message.event === "DatagridController:Click") {
                    $scope.config = sharedService.message.msg;
                }//处理datagrid的事件
            });
        }]);
    /**
     * 大指标显示控制器 创建人:林志敏
     */
    controllers.controller("LargeIndicatorController", [
        "$scope",
        "$log",
        "SearchSharedService",
        "KpiDetailService",
        "cache",
        "AttentionService",
        "$timeout",
        "$location",
        function($scope, $log, sharedService, detailService, cache, attentionService, $timeout, $location) {
            var context = this;
            var init = function() {
                var detail = detailService.get({
                    kpiId:$location.search()['kpiId']
                    //kpiId:"E3F94185A8E4195BE04014AC6120719A"
                }, function() {
                    if (angular.isUndefined(detail.error)) {
                        $scope.config = {
                            click:{
                                method:function(action) {
                                    var careStatus;
                                    if (detail.careStatus === 0)
                                        careStatus = 1;
                                    else
                                        careStatus = 0;
                                    switch (action) {
                                        case '关注':
                                            attentionService.update({
                                                id:cache.get("kpiId"),
                                                careStatus:careStatus
                                            }, function() {
                                                init();
                                            });
                                            break;
                                        case '申告':
                                            break;
                                    }
                                },
                                context:context
                            },
                            data:detail
                        };
                        sharedService.prepForBroadcast({event:"LargeIndicatorController:Refresh",msg:detail});
                    }
                    //$log.info(detail);
                });
            };
            $timeout(init);

        }]);


});