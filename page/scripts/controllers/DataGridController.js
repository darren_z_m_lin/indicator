define(["controllers/controllers","underscore", "services/services"], function (controllers,underscore) {

    controllers.controller("DataGridController", ["$scope","KpiUpDownInfoService","DataGridService","SearchSharedService","cache","$location", function ($scope, kpiUpDownInfoService,dataGridService,sharedService,cache,$location) {
       // console.log("DataGridController is call");
        /**
         * 详细页面展示数据DataGridController
         *
         *
         * -------------History------------------
         * DATE        AUTHOR    VERSION        DESCRIPTION
         * 2013-09-23   蔡书伟   V13.00.001      新增
         *
         *
         * author 蔡书伟
         */
        //"E3F94185A8E8195BE04014AC6120709A"
        $scope.currentFilterType = null;
        var requestData = null;
        var context = this;//上下文
        var pageNumber = 1;
        var total=57;
        var pageSize=10;
        var kpiID =  $location.search()['kpiId'];
        // 表filter对象
        var tableFilterObj = function () {
            return {
                "type":null,
                "value":[],
                "cssClass":[]
            };
        } ;
        var searchFilter = function() {
            return{
                "type":null,
                "value":[]
           };
        };
        var searchPager = function() {
            return{
                "pageNumber":0,
                "pageSize":0
            };
        };
        var searchFilterParams = function() {
           return {
               "kpiId":null,
               "filter":[],
               "pager":null
            }

        };
        // 表filter对象
        var tableHeadObj = function () {
            return {
                "name":null, //列名
                "column":null,//列名代表的字段名称
                "isDim":null,//0代表维度列，1代表指数值标列
                "isFormat":null,//0代表要格式化，1代表不
                "unit":null,//指标值单位
                "cssClass":null
            };
        };
        //列属性对象
        var tdStore = function () {
            return {
                "id":null,
                "rowspan":0,  //合并列值
                "cssClass":null,  //tdCss类名
                "value":null,    //td值
                "isDim":null,  //维度列可否下转
                "minIndex":null,  //值对应的列数组最小序号
                "headIndex":null, //所属列头序号
                "upTurnClass":"disabled", //上转class
                "downTurnClass":"disabled",  //下转class
                "upTurn":0,
                "downTurn":0,
                "spanClass":null
            };
        };
        var initTrableHeard = function(rsHead)
        {
            var tmpTableHead = [];
            angular.forEach(rsHead,function(rsHeadObj,index)    //迭代列数据,整理需合并的列数据
            {
               var  tmpTableHeadObj = new tableHeadObj();
                tmpTableHeadObj.name=rsHeadObj.name;
                tmpTableHeadObj.column=rsHeadObj.column;
                tmpTableHeadObj.isDim=rsHeadObj.isDim;
                tmpTableHeadObj.isFormat=rsHeadObj.isFormat;
                tmpTableHeadObj.unit= rsHeadObj.unit;
                if(rsHeadObj.isDim!=0)
                {
                    tmpTableHeadObj.cssClass="right_align";
                }
                else
                {
                    tmpTableHeadObj.cssClass="";
                }
               this.push(tmpTableHeadObj);
            },tmpTableHead);
            return  tmpTableHead;
        }
        var getGridDataParams = function(kpiId,tableFilter,pageNumber,pageSize)
        {
        	 var filterList = [];
             angular.forEach(tableFilter,function(filterObj,index)    //迭代列数据,整理需合并的列数据
             {
                 var searchFilterObj = new searchFilter();
                  searchFilterObj.type=filterObj.type;
                  searchFilterObj.value=filterObj.value;
                 filterList.push(searchFilterObj);
             });
            var params = new searchFilterParams();
            var pager = new searchPager();
            pager.pageSize = pageSize;
            pager.pageNumber = pageNumber;
            params.kpiId=kpiId;
            params.filter=filterList ;
            params.pager= pager;
             return params;
        }
        var getUpOrDownParams = function(kpiId,tableFilter,Tdobj,tableHead,pageNumber,pageSize)
        {
        	var filterList = [];
            var tmpFilter = angular.copy(tableFilter) ;
            var searchFilterObj = new searchFilter();
             var orderFilter = null;
             var columntype =  tableHead[Tdobj.headIndex].column;
             angular.forEach(tmpFilter,function(filterObj,index)    //迭代列数据
             {
            	 if(columntype==filterObj.type)
                 {
                     orderFilter =   filterObj;
                 }
             });
             if(orderFilter!=null)
             {
                 searchFilterObj.value = orderFilter.value
             }
            searchFilterObj.type = columntype;
            searchFilterObj.value.push(Tdobj.value);
            filterList.push(searchFilterObj);
            var params = new searchFilterParams();
            var pager = new searchPager();
            pager.pageSize = pageSize;
            pager.pageNumber = pageNumber;
            params.kpiId=kpiId;
            params.filter=filterList ;
            params.pager= pager;
             return params;
        }
        var searchGridData = function(kpiId,tableFilter)
        {
            var gridData = null;
            var params = getGridDataParams(kpiId,tableFilter,0,0);
            //获取数据
            dataGridService.post(params, function (rs) {
            	gridData =    rs;
            	return gridData;
            }, function (rs) {
            });
        }
        //设置下转上转filter
        var addFilter = function(Tdobj,tableHead)
        {               //过滤器对象
               var columntype =  tableHead[Tdobj.headIndex].column;
               var tmpfilter = angular.copy($scope.filter);
               if(typeof(tmpfilter)=="undefined")  //首次添加filter
               {
                   tmpfilter = [];
                   var  tmpFilterObj = new tableFilterObj();
                   tmpFilterObj.type =  columntype ;
                   tmpFilterObj.value.push(Tdobj.value) ;
                   tmpFilterObj.cssClass.push("filter_item last") ;
                   tmpfilter.push(tmpFilterObj);
               }
               else
               {
                   var isFirst = true;  //新filter
                   angular.forEach(tmpfilter,function(filterobj,index)
                   {
                       if(filterobj.type==columntype)
                       {
                           if(filterobj.value[filterobj.value.length-1]!=Tdobj.value)
                           {
                               filterobj.value.push(Tdobj.value);
                               filterobj.cssClass[filterobj.cssClass.length-1]="filter_item";      //设置倒数第二个filter的class
                               filterobj.cssClass.push("filter_item last") ;
                           }
                           isFirst = false;
                       }
                   });
                  if(isFirst)  //另外的条件
                  {
                      var  tmpFilterObj = new tableFilterObj();
                      tmpFilterObj.type =  columntype ;
                      tmpFilterObj.value.push(Tdobj.value) ;
                      tmpFilterObj.cssClass.push("filter_item last") ;
                      tmpfilter.push(tmpFilterObj);
                  }
               }
             $scope.currentFilterType =   columntype;
             $scope.filter =  tmpfilter;
        }
        //设置css中的class
        var getCssClass = function(tdHead)
        {
                if(tdHead.isDim===1)
                    return "right_align";
                else
                    return "rowspan";
        }
        //设置列属性对象
        var setDataTd = function(tdarray,index,rowspan,value)
        {
            var tmpData = angular.copy(tdarray);
            var tdObj =  new tdStore();
            tdObj.rowspan=rowspan;
            tdObj.value=value;
            tdObj.minIndex=index;
            tdObj.cssClass='';
            tmpData[index]=tdObj;
            return   tmpData ;
        }
        //清除相同列值
        var cleanDataTd =  function(tdarray,index)
        {
            var tmpData = angular.copy(tdarray);
            tmpData[index]=null;
            return   tmpData ;
        }

        //获取计算后的置顶维度
        var initMergeRow = function(tdData)
        {
            var tmpvalue = null;     //临时对比值
            var tmpTdData = angular.copy(tdData);
            var count = 0;        //rowSpan计数器
            var rowSpanIndex = 0;   //标示合并的列序号
            angular.forEach(tdData,function(value,index2)
            {
                if(index2==0)
                {
                    tmpvalue =  value;
                    rowSpanIndex =  index2;
                    count =1;
                    tmpTdData = setDataTd(tmpTdData,rowSpanIndex,count,tmpvalue) ;
                }
                else
                {
                    if(tmpvalue!=value)      //对比上一列值是否相同
                    {
                        tmpTdData = setDataTd(tmpTdData,rowSpanIndex,count,tmpvalue) ;
                        tmpvalue = value;
                        count =1;
                        if(index2==(tdData.length-1))      //最后一个值判断
                        {
                            tmpTdData = setDataTd(tmpTdData,index2,count,tmpvalue);
                        }
                        rowSpanIndex =  index2;
                    }
                    else
                    {
                        tmpTdData = cleanDataTd(tmpTdData,index2) ;
                        count ++;
                        if(index2==(tdData.length-1))   //最后一个值判断
                        {
                            tmpTdData = setDataTd(tmpTdData,rowSpanIndex,count,tmpvalue);
                        }
                    }
                }
            });
             return     tmpTdData;
        }              //获取计算后的置顶维度
        var initNotDimRow = function(tdData)
        {
            var tmpvalue = null;     //临时对比值
            var tmpTdData = [];
            var count = 1;        //rowSpan计数器
            var rowSpanIndex = 0;   //标示合并的列序号
            angular.forEach(tdData,function(value,lineIndex)
            {
                tmpTdData  = setDataTd(tmpTdData,lineIndex,count,value);
            });
            return     tmpTdData;
        }



        //初始化计算tabledata数据,合并元素
        var initMergeData = function(rsData,topMerge,initTopRpw)
        {
            var tableList = [];
            tableList.push(initTopRpw) ;
            var tmpInitTopRpw = angular.copy(initTopRpw);
            angular.forEach(rsData.data,function(rowData,rowIndex)    //迭代列数据,整理需合并的列数据
            {
                var tdRowData = [];
                if(rowIndex!=topMerge) //排除已计算的置顶维度列
                {
                    var tmpData = angular.copy(rowData);
                    angular.forEach(tmpInitTopRpw,function(obj,lineIndex)  //根据已算出来的置顶维度进行下级查找
                    {
                        if(obj!=null)
                        {
                            if(rsData.header[rowIndex].isDim==1)
                            {
                                 tdRowData = initNotDimRow(tmpData);
                            }
                            else
                            {
                                //返回array的第一个元素，设置了参数n，就返回前n个元素。
                                afterDate =  underscore.first(tmpData,obj.rowspan);     //得到rowsapan指定列数
                                tdRowData = tdRowData.concat(initMergeRow(afterDate));
                                tmpData.splice(0,obj.rowspan);   //每次处理完清除对应的列 ,避免循环
                            }
                        }
                    });
                    tableList.push(tdRowData)  ;
                }
            });
            return      tableList;
        }

        //获取页面表格对象
        var getGridTable = function(topRow,afterData,tableHead)
        {
            var table = [];
            var tr= [];
            var id_value =0;    //td在table里的唯一值
            for(var i=0;i<topRow.length;i++ )  //算行
            {
                tr[i]= [];
                for(var j =0;j<afterData.length;j++)   //算列
                {
                    if(afterData[j][i]!=null)
                    {
                        //通过传进的列头获取列头属性
                        afterData[j][i].id = id_value;
                        afterData[j][i].cssClass=getCssClass(tableHead[j]); //设置tdcss列属性
                        afterData[j][i].isDim = tableHead[j].isDim;  //设置列是否可以下转属性
                        afterData[j][i].headIndex = j;
                        if(tableHead[j].unit!=null)
                        {
                            if(tableHead[j].isFormat==0)
                            {
                                if (Number(afterData[j][i].value) >= 0)
                                {
                                    afterData[j][i].spanClass="value_up";
                                }
                                else
                                {
                                    afterData[j][i].spanClass="value_down";
                                }
                            }
                            afterData[j][i].value = afterData[j][i].value+tableHead[j].unit;
                        }
                        tr[i].push(afterData[j][i]);
                    }
                    id_value ++;
                }
                table.push(tr[i]) ;
            }
            return   table;
        }
        //获取当前Filter的信息
        var getCurrentFilterInfo = function(tableHead,tableFilter,currentFilterType)
        {
            var dimIndex = 0;
            var tmpfilter = angular.copy(tableFilter);
            var filterInfo  = {
                currentDimIndex:0,
                dimList:[]
            }
            angular.forEach(tableHead,function(headobj,index)
            {
                if(headobj.isDim==0)
                {
                    var viewHead =
                    {
                        type:null,
                        headIndex:0
                    };
                    if(currentFilterType!=null)
                    {
                        if(headobj.column==currentFilterType)
                        {
                            filterInfo.currentDimIndex =  dimIndex;
                        }
                    }
                    else
                    {
                        filterInfo.currentDimIndex =  0;
                    }
                    viewHead.type=headobj.column;
                    viewHead.headIndex = index;
                    filterInfo.dimList.push(viewHead);
                    dimIndex++;
                }
            });
            return filterInfo;
        }
        //根据图形触发获取图形请求后台数据所需的参数
        var getViewPparams = function(kpiId,filterInfo,tableFilter,type,filter_value,tableGrid,currentFilterType)
        {
            var searchFilterobj = new  searchFilter();
            var tdobj = new tdStore ();
            var tableFilterobj =  null;
            var nextDimObj =null;
            var tmpfilter = angular.copy(tableFilter);
            var filterExist =  false;
            var filterExistIndex = 0;
            if(!angular.isUndefined(tmpfilter))
            {
                if(tmpfilter.length==0)  //当filter已经初始化被清零后
                {
                    tableFilterobj = new tableFilterObj();
                    tableFilterobj.type = filterInfo.dimList[0].type;
                }
                else
                {
                    angular.forEach(tmpfilter,function(filterObj,index)    //迭代找出已存在的filter
                    {
                        if(filterObj.type==currentFilterType)
                        {
                            tableFilterobj =angular.copy(filterObj);
                            filterExist = true;
                            filterExistIndex = index;
                        }
                    });
                    if(!filterExist)//未被点击过
                    {
                        tableFilterobj = new tableFilterObj();
                        tableFilterobj.type = currentFilterType;
                    }
                }
            }
            else     //当filter未初始化
            {
                tableFilterobj = new tableFilterObj();
                tableFilterobj.type = filterInfo.dimList[0].type;
            }
            if(type==1)  //第一次获取下转
            {
                searchFilterobj.type = tableFilterobj.type;
                searchFilterobj.value =   tableFilterobj.value;
                if(filter_value!=searchFilterobj.value[searchFilterobj.value.length-1])
                {
                    searchFilterobj.value.push(filter_value);
                }
            }
            else
            {
                if((filterInfo.currentDimIndex+1) ==filterInfo.dimList.length)
                {
                    return false;
                }
                else
                {
                    searchFilterobj.type = tableFilterobj.type;
                    angular.forEach(filterInfo.dimList,function(dimObj,index)    //迭代找出下一个维度
                    {
                        if(dimObj.type==currentFilterType)
                        {
                            nextDimObj =dimObj;
                        }
                    });
                    filter_value =   tableGrid[0][nextDimObj.headIndex].value;
                    tdobj.value= filter_value;
                    tdobj.headIndex = nextDimObj.headIndex;
                    searchFilterobj.value = tableFilterobj.value;
                    searchFilterobj.value.push(filter_value);
                }
            }
            var  params =  getGridDataParams(kpiId,tmpfilter,0,0);
            var length = params.filter.length;
            if(length>0)
            {
                if(type==1)
                {
                    params.filter[filterExistIndex]  = searchFilterobj;
                }
                else
                {
                    if(filterExist)     //已经存在，清空
                    {
                        params.filter[filterExistIndex].value.push(filter_value);
                    }
                    else     //没添加过
                    {
                        params.filter.push(searchFilterobj);
                    }
                }
            }
            else
            {
                params.filter.push(searchFilterobj);
            }
            return  {params:params,tdObj:tdobj};
        }
        //更新页面数据展现表格
        var updateTable = function(params,isAddFilter,tbobj,tableHead,pageNumber) {
            var  rs =  dataGridService.post(params, function () {
                if(rs.data!=null)
                {
                    if(isAddFilter)   //    先设置filter
                    {
                        addFilter(tbobj,tableHead);
                        $scope.currentFilterType  =  tableHead[tbobj.headIndex].column;
                    }
                    initGridTable(rs,pageNumber);  //更新table
                }
            });
        }
        var viewClick = function(param)
        {
            var tmpList = param.split("/");
            var filter_value = null;
            var filterInfo = getCurrentFilterInfo($scope.tableHead,$scope.filter, $scope.currentFilterType);
            if(filterInfo.currentDimIndex==0)
                filter_value = tmpList[0];
            else
                filter_value = tmpList[tmpList.length-1];
            var kpiId = $location.search()['kpiId'];
            var  viewDataParamsObj = getViewPparams(kpiId,filterInfo,$scope.filter,1,filter_value,null,$scope.currentFilterType);
            viewDataParamsObj.tdObj.value= filter_value;
            viewDataParamsObj.tdObj.headIndex = filterInfo.dimList[filterInfo.currentDimIndex].headIndex;
            $scope.$apply(function(){
                //获取数据
                var upOrDownParams = getUpOrDownParams(kpiId,$scope.filter,viewDataParamsObj.tdObj,$scope.tableHead,0,0);
                kpiUpDownInfoService.post(upOrDownParams, function (rs) {
                    if(rs.down==1)  //可下转
                    {
                        updateTable(viewDataParamsObj.params,true,viewDataParamsObj.tdObj,$scope.tableHead,1);
                    }
                    else //不可下转
                    {
                        var tmpcurrentFilterType = filterInfo.dimList[filterInfo.currentDimIndex+1].type;
                        viewDataParamsObj = getViewPparams(kpiId,filterInfo,$scope.filter,2,filter_value,$scope.tableGrid,tmpcurrentFilterType);  //拼接新下转维度
                        updateTable(viewDataParamsObj.params,true,viewDataParamsObj.tdObj,$scope.tableHead,1);
                    }
                });
            })
        }
        // 根据获取的数据生成table
        var initGridTable = function(Griddata,pageNumber)
        {
            var topRow = Griddata.data[Griddata.topMerge];//获取置顶维度列数据
            var initTopRpw = initMergeRow (topRow);   //计算置顶维度列数据
            var afterData = initMergeData(Griddata,Griddata.topMerge,initTopRpw);
            var tableGrid =  getGridTable(topRow,afterData,Griddata.header);   //获取table表格
            var tableHead =  initTrableHeard(Griddata.header);     //获取table头
            $scope.tableHead = tableHead;
            $scope.tableGrid = tableGrid;

            $scope.pageNumber = pageNumber;
            $scope.total= Griddata.total;
            $scope.pageSize=Griddata.pageSize;
            $scope.pageClick = {method:function(pageParams){
             var postParams = getGridDataParams(kpiID,$scope.filter,pageParams.pageNumber,pageParams.pageSize);
             updateTable(postParams,false,null,null,pageParams.pageNumber);
            },context:this};
            var currentFilterInfo = getCurrentFilterInfo($scope.tableHead,$scope.filter,$scope.currentFilterType);
            if($scope.currentFilterType==null||$scope.filter.length==0) //设置当前currentFilterType
            {
                $scope.currentFilterType =  currentFilterInfo.dimList[0].type;
                currentFilterInfo.currentDimIndex =0;
            }
            var currentDimData = null;
            var tmpCurrentDimData = [];
            if(currentFilterInfo.currentDimIndex =0)
                currentDimData =   Griddata.data[currentFilterInfo.dimList[currentFilterInfo.currentDimIndex].headIndex];
            else
                currentDimData =   Griddata.data[currentFilterInfo.dimList[currentFilterInfo.currentDimIndex+1].headIndex];
                angular.forEach(currentDimData,function(dataObj,index)    //迭代列数据,整理需合并的列数据
                {
                    var tmp = Griddata.data[0][index] +"/"+ dataObj  ;
                    this.push(tmp);
                },tmpCurrentDimData);
            currentDimData = tmpCurrentDimData;
            var postdata = {
                data:{
                    x:{key:currentDimData},
                    y:[
                        {key:Griddata.data[2]}
                    ]
                },
                click:{
                    context:context,
                    method: viewClick
                }
           };
            var message = {event:"DatagridController:Click",msg:postdata};
            sharedService.prepForBroadcast(message);
        }

        //----------------------------------页面点击方法-------------------------------------------------
        $scope.getTurnInfo = function(obj)  //获取该项是否可下转
        {
        	var params = getUpOrDownParams(kpiID,$scope.filter,obj,$scope.tableHead,0,0) ;
        	var returnData = null;
            kpiUpDownInfoService.post(params, function (rs) {
        
        		angular.forEach($scope.tableGrid,function(trobj,index1)
        	            {
        	                angular.forEach(trobj,function(tdobj,index2)
        	                {
        	                         if(obj.id==tdobj.id)
        	                         {
        	                        	if(rs!=null)
        	                        	{   
        	                        		tdobj.upTurn=rs.up;
        	                        		tdobj.downTurn=rs.down;      	                        		
        	                        		if(rs.up!=0 )
        	                        		{
        	                        			 tdobj.upTurnClass = "";
        	                        		}
                                            else
                                            {
                                                tdobj.upTurnClass = "disabled";
                                            }
        	                        		if(rs.down!=0 )
        	                        		{
        	                        			 tdobj.downTurnClass = "";
        	                        		}
                                            else
                                            {
                                                tdobj.downTurnClass = "disabled";
                                            }
        	                        	}                            
        	                             return;
        	                         }
        	                });
        	            });
              });
        }
        //获取下转或上转数据
        $scope.goTurn = function(Tdobj,turnType)
        {
            if(turnType==2)   // 下转
            {
                addFilter(Tdobj,$scope.tableHead);
            }
            else     // 上转
            {
                var tmpfilter = [];
                var columntype =  $scope.tableHead[Tdobj.headIndex].column;
                angular.forEach($scope.filter,function(filterobj,index)
                {
                    if(filterobj.type==columntype)
                    {
                        var filterLength  =  filterobj.value.length-1;
                        filterobj.value = underscore.first( filterobj.value,filterLength) ;    // 返回前n个元素。
                        if(filterobj.value.length!=0)
                        {
                            filterobj.cssClass = underscore.first( filterobj.cssClass,filterLength);     // 返回前n个元素。
                            filterobj.cssClass[filterobj.cssClass.length-1]  = "filter_item last";   //设置最后一个filter的css
                            tmpfilter.push(filterobj);
                        }
                    }
                    else
                    {
                        tmpfilter.push(filterobj);
                    }
                });
                $scope.currentFilterType =   columntype;
                $scope.filter =  tmpfilter;
            }
            var params = getGridDataParams(kpiID,$scope.filter,0,0);
            //获取数据
            updateTable(params,false,null,null,1);
        }
        //删除filter
        $scope.delfilter = function(delfiter,delIndex)
        {
            var tmpFilter = angular.copy($scope.filter);
            var filter =  [];
            angular.forEach($scope.filter,function(filterobj,index)
            {
                if(filterobj.type==delfiter.type)
                {
                    var filterLength  =  filterobj.value.length;
                    tmpFilter[index].value = underscore.first( tmpFilter[index].value,delIndex) ;    // 返回前n个元素。
                    if(tmpFilter[index].value.length!=0)
                    {
                        tmpFilter[index].cssClass = underscore.first( tmpFilter[index].cssClass,delIndex);     // 返回前n个元素。
                        tmpFilter[index].cssClass[tmpFilter[index].cssClass.length-1]  = "filter_item last";   //设置最后一个filter的css
                        filter.push(tmpFilter[index]);
                    }
                }
                else
                {
                    filter.push(filterobj);
                }
            });
            $scope.filter = filter;
            var params = getGridDataParams(kpiID,$scope.filter,0,0);
            //获取数据
            updateTable(params,false,null,null,1);

        }
        //获取数据
        var params = getGridDataParams(kpiID,$scope.filter,0,0);
        updateTable(params,false,null,null,1);
        //获取数据

}]);


});