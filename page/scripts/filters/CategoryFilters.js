/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["filters/filters"], function(filters) {
    /**
     * 创建人:林志敏
     * 日期:2013-9-16
     */
    filters.filter("trend", function() {
        return function(input) {
            //console.log(input);

            if (angular.isUndefined(input) || input === null)
                return "";
            if (input.toString().charAt(0) === '-')
                return "value_down";
            else
                return "value_up";
        };
    });
    /**
     * 创建人:林志敏
     * 日期:2013-9-16
     */
    filters.filter("kpis", function() {
        return function(input) {
            //console.log(input);
            if (angular.isUndefined(input))
                return "";
            if(input===null)
                return "";
            var wholePartArray = [];
            var wholeAndFractionArray = [];
            var wholePartString;
            var fractionPartString;
            var addWholePartBoolean = false;
            var wholeFractionArray = input.toString().split(".");
            wholePartString = wholeFractionArray[0];
            fractionPartString = wholeFractionArray[1];
            if((!angular.isUndefined(fractionPartString))&&(fractionPartString.length>2)){//当小数部分大于3位数时 对小数进行四舍五入处理
                if(parseInt(fractionPartString.substr(2,1))>4){//如果小数点第三位数大于4
                    if(parseInt(fractionPartString)===99){//当小数前两位数等于99的时候 在四舍五入时要让整数部分加1
                        addWholePartBoolean = true;
                    }
                    fractionPartString = (parseInt(fractionPartString.substr(0,2))+1)+"";//保留两位小数并加1

                }else{//如果小数点第三位数小于4
                    fractionPartString = fractionPartString.substr(0,2);//保留两位小数
                }
            }
            if(addWholePartBoolean){//整数部分加一
                wholePartString = (parseInt(wholePartString)+1)+"";
            }
            angular.forEach(wholePartString.split("").reverse(), function(item, index) {
                this.push(item);
                if (index % 3 === 2) {
                    this.push(",");
                }
            }, wholePartArray);//添加“，”到整数部分
            if (wholePartArray[wholePartArray.length - 1] === ",")
                wholePartArray.splice(wholePartArray.length - 1, 1);
            wholeAndFractionArray.push(wholePartArray.reverse().join(''));
            if (!angular.isUndefined(fractionPartString)) {//当有小数部分的时候 显示整数部分和小数部分
                wholeAndFractionArray.push(fractionPartString);
                return wholeAndFractionArray.join(".");
            } else {//当有小数部分的时候 只显示整数
                return wholeAndFractionArray.join("");
            }

        };
    });
    /**
     * 创建人:林志敏
     * 日期:2013-9-16
     */
    filters.filter("attention", function() {
        return function(input) {
            //console.log(input);
            if (input === 1)
                return "取消关注";
            else
                return "加关注";
        };
    });
    /**
     * 创建人:林志敏
     * 日期:2013-9-16
     */
    filters.filter("attentionIcon", function() {
        return function(input) {
            //console.log(input);
            if (input === 1)
                return "";
            else
                return "icon-plus";
        };
    });
    /**
     * 创建人:林志敏
     * 日期:2013-9-16
     */
    filters.filter("largeIndicatorCareStatus", function() {
        return function(input) {
            //console.log(input);
            if (input === 1)
                return "";
            else
                return "btn-success";
        };
    });
    /**
     * 创建人:林志敏
     * 日期:2013-9-16
     */
    filters.filter("kpiSelection", function() {
        return function(input) {
            //console.log(input);
            if (input === true)
                return "selected";
            else
                return "";
        };
    });
     /**
     * 创建人:林志敏
     * 日期:2013-9-16
     */
    filters.filter("iconSelected", function() {
        return function(input) {
            //console.log(input);
            if (input === true)
                return "icon-check-sign";
            else
                return "icon-check-empty";
        };
    });

});