/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
// the app/scripts/main.js file, which defines our RequireJS config
require.config({
    packages: [
       /* {
            name:'echarts',
            location:'vendor/echart',
            main:'echarts'
        },*/
       {
            name: 'echarts',
            location: 'vendor/echarts',
            main: 'echarts'
        },
        {
            name: 'zrender',
            location: 'vendor/zrender/', // zrender与echarts在同一级目录
            main: 'zrender'
        }
    ],
    paths: {
        app: 'app',
        angular: 'vendor/angular',
        jquery: 'vendor/jquery',
        domReady: 'vendor/domReady',
        bootstrap: 'vendor/bootstrap.min',
        angularResource: 'vendor/angular-resource',
        underscore:'vendor/underscore-min'
    },
    shim: {
        angular: {
            deps: [ 'jquery'],
            exports: 'angular'
        },
        bootstrap: {
            deps: [ 'jquery'],
            exports: 'bootstrap'
        },
        angularResource: {
            deps: ['angular'],
            exports: 'angularResource'
        },
        echarts:{
            deps:['zrender'],
            exports:'zrender'
        },
        underscore: {
            exports: '_'
        },
        underscorestring: {
            deps: ['underscore'],
            init: function (_) {
                //Mixin plugin to namespace
                _.mixin(_.str.exports());

                return _;
            }
        }

    }
});

require([
    'angular',
    'app',
    'domReady',
    'jquery',
    'services/services',
    'services/CategoryServices',
    'controllers/CategoryControllers',
    'controllers/MenuController',
    'controllers/HotKpiController',
    'controllers/DataGridController',
    'controllers/TagController',
    'controllers/controllers',
    'directives/directives',
    'directives/CategoryDirectives',
    'directives/ui',
    //'directives/uiIf',
    'directives/filter',
    'directives/line',
    'directives/menu',
    'directives/keyword',
    'directives/kpiHot',
    'directives/indicator',
    'directives/pager',
    'directives/kpis',
    'directives/mainMenu',
    'directives/location',
    'directives/basicInfo',
    'directives/comments',
    'directives/criteria',
    'directives/largeIndicator',
    'directives/largeChart',
    'directives/recommends',
    'directives/dataGrid',
    'directives/indicatorGrid',
    'directives/indicatorGridToolbar',
    'directives/indicatorGridEditDialog',
    'directives/indicatorTagEditDialog',
    'filters/filters',
    'filters/CategoryFilters',
    'angularResource',
    'bootstrap',
    'underscore'
    // Any individual controller, service, directive or filter file
    // that you add will need to be pulled in here.
],
    function (angular, app, domReady) {
        'use strict';
        app.config(['$routeProvider', '$httpProvider',
            function ($routeProvider, $httpProvider) {
                $httpProvider.defaults.useXDomain = true;
                delete $httpProvider.defaults.headers.common['X-Requested-With'];
                $httpProvider.responseInterceptors.push('myHttpInterceptor');
                $routeProvider.when('/index', {
                    templateUrl: 'views/index.html',
                    controller: "RootCtrl"
                }).when('/cat_manager', {
                        templateUrl: 'views/category_manager.html',
                        resolve: {
                            categories: ["MultipleCategoryLoader", function (MultipleCategoryLoader) {
                                return MultipleCategoryLoader();
                            }]
                        },
                        controller: 'CategoriesCtrl'
                    }).when('/detail',{
                        templateUrl:'views/detail.html',
                        controller:'DetailCtrl'
                    }).when("/indicator_manager",{
                        templateUrl: 'views/indicator_manager.html',
                         controller: "IndicatorManagerCtrl"
                    }).otherwise({
                         templateUrl: 'views/index.html',
                         controller: "RootCtrl"
                    });
            }
        ]);
        domReady(function () {
            angular.bootstrap(document, ['myApp']);
            // The following is required if you want AngularJS Scenario tests to work
            //$('html').addClass('ng-app: MyApp');
        });
        /*$(function() {

         //判断是否有下级，如果有，自动添加展开的按钮
         $(".tree_item").each(function(){
         if( $(this).find(">.manage_tree_ul").size()>0 ){
         $(this).find(".tree_item_box:first").append("<div class='expand_trigger'></div>");
         }
         })

         //对所tree_item添加clearfix的类用于清除浮动，并赋予所属层级，以标记颜色
         $(".tree_item").addClass("clearfix");
         $("#manage_tree_ul>.tree_item").addClass("item_lv1");
         $("#manage_tree_ul>.tree_item>.manage_tree_ul>.tree_item").addClass("item_lv2");
         $("#manage_tree_ul>.tree_item>.manage_tree_ul>.tree_item .tree_item").addClass("item_lv3");

         //展开按钮的动作
         $(".expand_trigger").click(function() {
         $(this).parent(".tree_item_box").next(".manage_tree_ul").slideToggle("fast");
         $(this).toggleClass("trigger_opened");
         });

         //IE6去死
         if ($.browser.msie && $.browser.version==6.0) {

         //重要！鼠标移上出现节点功能
         $(".item_wrapper").hover(
         function () {
         $(this).css("marginLeft","0");
         },
         function () {
         $(this).css("marginLeft","-100px");
         }
         );
         //鼠标移高亮节点功能
         $(".func_btn").hover(
         function () {
         $(this).css("backgroundColor","#333");
         },
         function () {
         $(this).css("backgroundColor","transparent");
         }
         );
         //修改每棵树第一个节点的样式（IE6以上支持直接CSS选择第一个元素）
         $("#manage_tree_ul").find(".manage_tree_ul").each(function(){
         $(this).find(".tree_item:first").find(">.tree_item_box").addClass("fir");
         })

         }

         //修改每棵树最后一个节点的样式（IE9以上才支持直接CSS选择最后一个元素）
         if ($.browser.msie && $.browser.version<=8.0) {
         $("#manage_tree_ul").find(".manage_tree_ul").each(function(){
         $(this).find(">.tree_item:last").find(".tree_item_box").addClass("last");
         })
         }

         });*/
    }
);
