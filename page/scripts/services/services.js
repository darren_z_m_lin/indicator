/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
define(["angular","angularResource"],function(angular){
    "use strict";
    return angular.module("services",["ngResource"]);
});