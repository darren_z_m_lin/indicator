/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(['services/services'], function (services) {
    var address="172.20.33.39";
    //var address = "172.20.32.251";
    //var address="172.20.10.247";
    var port = "8080";
    var app = "IndecatorLib";
    var servicesUrl = 'http://' + address + '\\:' + port + '/' + app;
    //var servicesUrl =  '../..';
    services.factory('Category', ['$resource', function ($resource) {
        return $resource(servicesUrl + '/mvc/tnBaseKPICate/:cateId', {cateId: '@cateId' });
    }]);//数据源
    services.factory('tagManagementService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/kpiGiveTag', {}, {
            get:{method:"POST",isObject:true}
        });
    }]);
    services.factory('KpiManagementService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/kpiAddOrUpdate', {}, {
            save:{method:"POST",isArray:true}
        });
    }]);
    services.factory('KpiInfoService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/kpiPageList', {}, {
            get:{method:"POST",isArray:true}
        });
    }]);
    services.factory('DimensionService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/dimensionList', {}, {
            get:{method:"POST",isArray:true}
        });
    }]);
    services.factory('RootCategoriesService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/tnBaseKPICate', {}, {
            get:{method:"POST",isArray:true}
        });
    }]);
    services.factory('KpisByCateIdService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/tnBaseKPICate/:cateId', {cateId: '@cateId' });
    }]);
    services.factory('SortedKpisService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/searchManagerCateKpi', {}, {get:{
            method:"POST", isObject:true
        }});
    }]);
    services.factory('UpdateCateService', ['$resource',function($resource) {
        return $resource(servicesUrl + '/mvc/updateData', {}, {update:{
            method:"POST", isObject:true
        }});
    }]);
    services.factory('KpiDetailService', ["$resource",function($resource) {
        return $resource(servicesUrl + '/mvc/kpiDetail/', {}, {
            get:{method:'post',isObject:true}
        });
    }]);
    services.factory("KpiCommentService", ["$resource",function($resource) {
        return $resource(servicesUrl + '/mvc/kpiComment/', {}, {
            query:{method:'post',isObject:true}
        });
    }]);
    services.factory("AddKpiCommentService", ["$resource",function($resource) {
        return $resource(servicesUrl + '/mvc/addKpiComment/', {}, {
            put:{method:'post',isObject:true}
        });
    }]);
    services.factory("RelativeKpiService", ["$resource",function($resource) {
        return $resource(servicesUrl + '/mvc/kpiRelateData', {}, {
            query:{method:'post',isArray:true}
        });
    }]);
    services.factory('DataGridService', ['$resource',function($resource) {

        return $resource(servicesUrl + '/mvc/kpiDataView/', {}, {
            post:{method:'post',isObject:true}
        });
    }]);
    services.factory('KpiUpDownInfoService', ['$resource',function($resource) {

        return $resource(servicesUrl + '/mvc/kpiUpOrDownInfo', {}, {
            post:{method:'post',isObject:true}
        });
    }]);
    services.factory('KpiUpDownInfoService', ['$resource',function($resource) {

        return $resource(servicesUrl + '/mvc/kpiUpOrDownInfo', {}, {
            post:{method:'post',isObject:true}
        });
    }]);
    services.factory("BasicKpiInfoService", ["$resource",function($resource) {
        return $resource(servicesUrl + '/mvc/kpiinfo', {}, {
            get:{method:'post',isObject:true}
        });
    }]);
    services.factory('SearchService', ['$resource',function($resource) {
        return $resource(servicesUrl + "/mvc/searchIndex", {}, {search:{method:'POST',isObject:true}});
    }]);
    services.factory('AttentionService', ['$resource',function($resource) {
        return $resource(servicesUrl + "/mvc/kpiAttention", {}, {update:{method:'POST',isObject:true}});
    }]);
    services.factory('MenuService', ['$resource',function ($resource) {
        return $resource(servicesUrl + "/mvc/tnBaseKPICate/dato", {}, {post:{method:'POST',isObject:true}});

    }]);//数据源
    services.factory('HotKpiService', ['$resource',function ($resource) {
        return $resource(servicesUrl + '/mvc/tnbaseKPIInfo/topKpi', {}, {post:{method:'POST',isObject:true}});
    }]);//数据源
    services.factory('MultipleCategoryLoader', ['Category', '$q', function (Category, $q) {
        return function () {
            var delay = $q.defer();
            Category.query(function (categories) {
                delay.resolve(categories);
            }, function () {
                delay.reject('Unable to fetch categories');
            });
            return delay.promise;
        };
    }]);//获取分类数据集合
    services.factory('CategoryLoader', ['Category', '$route', '$q', function (Category, $route, $q) {
        return function () {
            var delay = $q.defer();
            Category.get({id: $route.current.params.id}, function (category) {
                delay.resolve(category);
            }, function () {
                delay.reject('Unable to fetch category');
            });
            return delay.promise;
        };
    }]);//获取单个分类数据
    services.factory('TreeService', ["cache", "expand",function (cache, expand) {
        var TreeNode = function (item) {
            return {
                /*parent: category,*/
                parentID: item.parentID,
                cateId: item.cateId,
                cateName: item.cateName,
                cateNO: item.cateNO,
                //ord: category.ord,
                children: []
            };
        };//一个树节点的数据结构
        var createChildren = function (currentTreeNode) {
            angular.forEach(cache.get("array"), function (item, index) {
                if (currentTreeNode.cateId === item.parentID) {
                    var treeNode = new TreeNode(item);
                    treeNode = createChildren(treeNode);
                    this.push(treeNode);
                }
            }, currentTreeNode.children);
            return currentTreeNode;
        }; //创建一个节点的子树
        var getTree = function () {
            var tree = [];//创建用于存放多颗树根节点
            angular.forEach(cache.get("array"), function (item, index) {
                if (item.parentID === "9999") {
                    var treeNode = new TreeNode(item);
                    treeNode = createChildren(treeNode);
                    this.push(treeNode);
                }
            }, tree); //构造多颗树的根节点
            return tree;
        };//创建树
        var init = function (array) {
            if (!angular.isArray(array))
                throw("data is not array");
            cache.put("array", array);
            return getTree();
        };//初始化数组为树结构
        var findTreeById = function(currentId) {
            var found = [];
            found.push(currentId);
            angular.forEach(cache.get("array"), function(item, index) {
                if (currentId === item.parentID) {
                    this.concat(findTreeById(item.cateId));
                }
            }, found);
            return found;
        };//找出某个节点id所属的子树节点的id集合
        var addTreeNode = function (item) {
            if (isExist(item.cateId))
                throw("TreeNode is exist");
            var tmp = cache.get("array");
            tmp.push(item);
            cache.put("array", tmp);
            return getTree();
        };//添加树节点
        var updateTreeNode = function (item) {
            var tmp = cache.get("array");
            angular.forEach(tmp, function (items, index) {
                if (items.cateId === item.cateId) {
                    items.cateName = item.cateName;
                    items.cateNO = item.cateNO;
                }
            });
            cache.put("array", tmp);
            return getTree();
        };//添加树节点
        var isExist = function (id) {
            var exist = false;
            angular.forEach(cache.get("array"), function (item, index) {
                if (item.cateId === id) {
                    exist = true;
                }
            });
            return exist;
        }; //检验是否存在输入的id
        var removeByIds = function(ids) {
            var tmp = angular.copy(cache.get("array"));
            angular.forEach(ids, function(id, i1) {
                angular.forEach(cache.get("array"), function(item, i2) {
                    if (item.cateId === id) {
                        tmp.splice(i2, 1);
                    }
                });
            });
            cache.put("array", tmp);
            return getTree();
        }; //在缓存中根据id数组删除树节点
        var removeById = function(currentId) {
            var tmp = findTreeById(currentId);
            removeByIds(tmp);
            angular.forEach(tmp, function(id, index) {
                expand.removeExpand(id);
            });
            return getTree();
        };// 删除从某个节点以及它的子树所有节点
        return{
            init: init,//初始化构建树的原始数据
            removeById:removeById,//根据id删除树节点
            addTreeNode:addTreeNode,//添加树节点
            getTree:getTree,//获取构建后的:
            updateTreeNode:updateTreeNode
        };
    }]);//数组转换树以及对树节点操作
    services.factory('myHttpInterceptor', ["$q", "TreeService", function ($q, treeService) {
        return function (promise) {
            return promise.then(function (response) {
                // do something on success

                if (response.config.url === servicesUrl.replace('\\', '') + '/mvc/tnBaseKPICate' && response.config.method === 'GET') {
                    // Validate response if not ok reject
                    //var data = examineJSONResponse(response); // assumes this function is available
                    //console.log(data);
                    /*if (!data)
                     return $q.reject(response);*/
                    treeService.init(response.data);
                }
                return response;
            }, function (response) {
                // do something on error
                return $q.reject(response);
            });
        };
    }]);//这个监听器用于获取分类数据请求返回的数据并把数据放进TreeService
    services.factory("cache", ["$cacheFactory",function($cacheFactory) {
        return $cacheFactory("cache"); //创建应用全局缓存
    }]); //全局缓存
    services.factory("expand", ["cache",function(cache) {
        cache.put("expand", []);
        var addExpand = function(id) {
            if (isExist(id))
                return;
            var tmp = cache.get("expand");
            tmp.push(id);
            cache.put("expand", tmp);
        };
        var removeExpand = function(id) {
            if (!isExist(id)) return;
            var tmp = cache.get("expand");
            angular.forEach(tmp, function(item, index) {
                if (id === item)
                    tmp.splice(index, 1);
            });
            cache.put("expand", tmp);
        };
        var isExist = function(id) {
            var here = false;
            var tmp = cache.get("expand");
            angular.forEach(tmp, function(item, index) {
                if (item === id)
                    here = true;
            });
            return here;
        };
        var getExpand = function() {
            return cache.get("expand");
        };
        return {
            addExpand:addExpand, //添加展开树节点记录
            removeExpand:removeExpand,//删除展开树节点记录
            getExpand:getExpand  //展开树节点的全部记录
        };
    }]);//树节点展开收缩操作记录
    services.factory('SearchSharedService', function($rootScope) {
        var sharedService = {};

        sharedService.message = '';

        sharedService.prepForBroadcast = function(msg) {
            this.message = msg;
            this.broadcastItem();
        };

        sharedService.broadcastItem = function() {
            $rootScope.$broadcast('handleBroadcast');
        };

        return sharedService;
    }); //多个控制器通讯所用的组件


});