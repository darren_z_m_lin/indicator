/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
// The app/scripts/app.js file, which defines our AngularJS app
define(['angular',
    'controllers/controllers',
    'controllers/CategoryControllers',
    'services/services',
    'services/CategoryServices',
    'angularResource',
    'directives/directives',
    'directives/CategoryDirectives',
    'directives/ui',
    'directives/filter',
    'directives/criteria',
    'directives/keyword',
    'directives/kpis',
    'directives/pager',
    'directives/mainMenu',
    'directives/location',
    'directives/basicInfo',
    'directives/comments',
    'directives/largeIndicator',
    'directives/largeChart',
    'directives/recommends',
    'directives/indicatorGrid',
    'directives/indicatorGridToolbar',
    'directives/indicatorGridEditDialog',
    'directives/indicator',
    'directives/line',
    //'directives/uiIf',
    'directives/dataGrid',
'filters/filters','filters/CategoryFilters'], function (angular) {
    return angular.module('myApp', ['filters','controllers', 'services', 'ngResource', 'directives','ui']);
});