/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
define(["directives/directives","services/services","services/CategoryServices"], function (directives) {
    /**
     * 创建人:林志敏
     */
    directives.directive("init", ["expand", function (expand) {
        return function postLink(scope, iElement, iAttrs) {
            //判断是否有下级，如果有，自动添加展开的按钮
            angular.element(".tree_item").each(function () {
                var data = angular.element(this).attr("data");
                if (angular.element(this).find(">.manage_tree_ul li").size() > 0
                    /*&&
                    angular.element(this).find(".tree_item_box:first>.expand_trigger").size() === 0*/
                    ) {
                    angular.element(this).find(".tree_item_box:first").append("<div class='expand_trigger'></div>");
                    //angular.element(this).find(".tree_item_box:first>.expand_trigger").attr("data", data);
                }
            });

            //对所tree_item添加clearfix的类用于清除浮动，并赋予所属层级，以标记颜色
            angular.element(".tree_item").addClass("clearfix");
            angular.element("#manage_tree_ul>.tree_item").addClass("item_lv1");
            angular.element("#manage_tree_ul>.tree_item>.manage_tree_ul>.tree_item").addClass("item_lv2");
            angular.element("#manage_tree_ul>.tree_item>.manage_tree_ul>.tree_item .tree_item").addClass("item_lv3");

            //展开按钮的动作
            angular.element(".expand_trigger").click({expand:expand},function (e) {
                angular.element(this).parent(".tree_item_box").next(".manage_tree_ul").slideToggle("fast");
                angular.element(this).toggleClass("trigger_opened");
                //var data = angular.element(this).attr("data");
                //e.data.expand.addExpand(data);
            });

            //刷新树之后还原状态
            var ids = expand.getExpand();//获取上次打开的树节点id
            var select = [];
            angular.forEach(ids, function (id, index) {
                this.push(".expand_trigger[data=" + id + "]");
            }, select);
            select = select.join(",");//根据获取的上次打开的树节点id集合用jquery选择器找出相关树节点展开器
            angular.element(select).toggleClass("trigger_opened");//打开展开器

            //IE6去死
            if (angular.element.browser.msie && angular.element.browser.version == 6.0) {

                //重要！鼠标移上出现节点功能
                angular.element(".item_wrapper").hover(
                    function () {
                        angular.element(this).css("marginLeft", "0");
                    },
                    function () {
                        angular.element(this).css("marginLeft", "-100px");
                    }
                );
                //鼠标移高亮节点功能
                angular.element(".func_btn").hover(
                    function () {
                        angular.element(this).css("backgroundColor", "#333");
                    },
                    function () {
                        angular.element(this).css("backgroundColor", "transparent");
                    }
                );
                //修改每棵树第一个节点的样式（IE6以上支持直接CSS选择第一个元素）
                angular.element("#manage_tree_ul").find(".manage_tree_ul").each(function () {
                    angular.element(this).find(".tree_item:first").find(">.tree_item_box").addClass("fir");
                })

            }

            //修改每棵树最后一个节点的样式（IE9以上才支持直接CSS选择最后一个元素）
            if (angular.element.browser.msie && angular.element.browser.version <= 8.0) {
                angular.element("#manage_tree_ul").find(".manage_tree_ul").each(function () {
                    angular.element(this).find(">.tree_item:last").find(".tree_item_box").addClass("last");
                })
            }


        };
    }]); //可以作为directive编写范例，但没有被利用。因为发现不适合嵌套模板生成页面后初始化过程。


});