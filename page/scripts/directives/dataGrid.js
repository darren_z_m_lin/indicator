/**
 *数据展现directives
 * DATE        AUTHOR    VERSION        DESCRIPTION
 * 2013-09-16   蔡书伟   V13.00.001      新增
 */
define(["directives/ui","services/CategoryServices"], function (ui) {
    ui.directive("datagrid", ["$controller",function ($controller) {

        return {
            restrict: 'A',
            templateUrl: '.././page/scripts/directives/dataGrid.html',
            scope: {
                controller: "@controller"
            },
            link: function (scope, elem, attrs) {
                //$log.debug(scope);
                var controller = $controller(scope['controller'],{
                    $scope:scope
                });
                //$log.debug(scope);
            }
        };
    }])
});