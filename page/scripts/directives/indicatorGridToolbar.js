/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("indicatorGridToolbar", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/indicatorGridToolbar.html',
                scope: {
                    selected:"=selected",
                    add:"&",
                    remove:"&",
                    search:"&",
                    options:"="
                },
                link: function (scope, elem, attrs) {
                    scope.indicatorName = "";
                    scope.indicatorCode = "";
                    scope.query = function(){
                        scope.search({params:{
                            indicatorName:scope.indicatorName,
                            indicatorCode:scope.indicatorCode,
                            cateId:scope.selected}});
                    };
                }
            };
        }]);
});