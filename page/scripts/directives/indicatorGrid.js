/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("indicatorGrid", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/indicatorGrid.html',
                scope: {
                    data:"=data",
                    select:"&",
                    edit:"&",
                    tag:"&"
                },
                link: function (scope, elem, attrs) {
                    $log.info(scope);
                }
            };
        }])
});