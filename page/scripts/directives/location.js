/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
define(["directives/ui","services/CategoryServices"], function (ui) {
    /**
     * 关键字标签 创建人:林志敏
     */
    ui.directive("location", ["$controller","$log","SearchSharedService",function ($controller,$log,sharedService) {

        return {
            restrict: 'A',
            templateUrl: '.././page/scripts/directives/location.html',
            scope: {
                controller: "@controller"
            },
            link: function (scope, elem, attrs) {
                var isolateScope = scope.$new(true);
                var func = function(){
                    //$log.info("come from location context");
                };
                var context = this;
                isolateScope.config = {
                    click:{
                        method:func,
                        context:context
                    },
                    links:[
                        {name:"cat1",id:"cat1"},{name:"cat2",id:"cat2"},{name:"cat3",id:"cat3"}
                    ]
                };
                var controller = $controller(scope["controller"], {$scope:isolateScope});
                isolateScope.$watch("config",function(newVal){
                    var links = [];
                    angular.forEach(newVal.links,function(item){
                        links.push({name:item.name,click:angular.element.proxy(newVal.click.method,newVal.click.context,item),active:""});
                    });
                    links[links.length-1].active = "active";
                    scope.config = {
                        links:links
                    };
                });
                
            }
        };
    }])
});