/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
define(["directives/ui","services/CategoryServices"], function (ui) {
    /**
     * 关键字标签 创建人:林志敏
     */
    ui.directive("keyword", ["$controller",function ($controller) {

        return {
            restrict: 'A',
            templateUrl: '.././page/scripts/directives/keyword.html',
            scope: {
                controller: "@controller"
            },
            link: function (scope, elem, attrs) {
                //$log.debug(scope);
                var controller = $controller(scope['controller'],{
                    $scope:scope
                   });
                
                //$log.debug(scope);
            }
        };
    }])
});