/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
define(["directives/ui","services/services"], function (ui) {
    /**
     * 指标列表标签 创建人:林志敏
     */
    ui.directive("kpis", ["$controller", "$log",
        function ($controller, $log) {

        return {
            restrict: 'A',
            replace:true,
            templateUrl: '.././page/scripts/directives/kpis.html',
            scope: {
                controller:"@controller"
            },
            link: function (scope, elem, attrs) {
               //$log.debug(scope);
               var controller = $controller(scope['controller'],{$scope:scope});

            }
        };
    }])
});