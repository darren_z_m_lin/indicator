/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("comments", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/comments.html',
                scope: {
                    controller:'@controller'
                },
                link: function (scope, elem, attrs) {
                    var isolateScope = scope.$new(true);
                    isolateScope.config = {
                        total:30,pageNumber:1,avgScore:4,
                            data:[
                                {
                                    "commentId":"7eac538d-faa3-4741-b0ea-c6b9adb43b7c",//评论ID
                                    "userId":"bc7dda5e-1a78-4236-b9f2-5fd5027e51e3",//评论人ID
                                    "userName":"小明",//评论人名称
                                    "review":"dsfnosdfg",//评论内容
                                    "star":5,//用户评分星星数
                                    "time":"2013-08-30 15:25",//评论时间
                                    "userImage":"http://XXXXXXX.png"
                                },
                                {
                                    "commentId":"7eac538d-faa3-4741-b0ea-c6b9adb43b7c",//评论ID
                                    "userId":"bc7dda5e-1a78-4236-b9f2-5fd5027e51e3",//评论人ID
                                    "userName":"小明",//评论人名称
                                    "review":"dsfnosdfg",//评论内容
                                    "star":5,//用户评分星星数
                                    "time":"2013-08-30 15:25",//评论时间
                                    "userImage":"http://XXXXXXX.png"
                                }
                            ]
                        };
                    var controller = $controller(scope["controller"], {$scope:isolateScope});
                    isolateScope.$watch("config", function(config) {
                        scope.config = config;
                    });
                }
            };
        }])
});
