/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("pager", ["$controller", "$log",
        function ($controller, $log) {
            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/pager.html',
                scope: {
                    controller:'@controller',
                    pageNumber:'@pageNumber',
                    pageSize:'@pageSize',
                    total:'@total',
                    click:'=click'
                },
                link: function (scope, elem, attrs) {
                     var config = {
                         total:0,
                         pageSize:10,
                         pageNumber:1,
                         click:{method:function() {
                        //$log.info("i come from pager context");
                    },context:this}};//初始化用户控制器设置对象
                    if(!isNaN(parseInt(scope.total))){
                        config.total = scope.total;
                    }
                    if(!isNaN(parseInt(scope.pageNumber))){
                        config.pageNumber = scope.pageNumber;
                    }
                    if(!isNaN(parseInt(scope.pageSize))){
                        config.pageSize = scope.pageSize;
                    }
                    if(!angular.isUndefined(scope.click)){
                        config.click = scope.click;
                    }
                    //分页算法类
                    var pagesSplit = function(config_curpage,config_pages )
                    {
                        var begin=1;//开始的页数
                        var pages = 0;
                        var pageNow = 1;
                        if(!isNaN(parseInt(config_curpage))){
                            pageNow = angular.copy(config_curpage);
                        }
                        if(!isNaN(parseInt(config_pages))){
                            pages =  angular.copy(config_pages);
                        }
                        if(pageNow<1)
                            pageNow=1;
                        //var next=pageNow+1;    //下一页
                        //var previous=pageNow-1;  //上一页
                        var startPage=(pageNow+5)>pages?pages-10:pageNow-5;
                        var endPage=pageNow<5?10:pageNow+5;
                        if(startPage<1)
                            startPage=1;
                        if(pages<endPage)
                            endPage=pages;
                        return {startPage:startPage, endPage:endPage};
                    }
                    var getData = function(config) {
                        if(angular.isUndefined(config.click))
                            config.click = {method:function(){},context:this};
                        var data = {};
                        data.pageTotal = config.total / config.pageSize;
                        data.pageTotal = data.pageTotal << 0;   //去除小数点
                        if (config.total % config.pageSize > 0) //计算总页数
                            data.pageTotal++;
                        data.pages = [];//储存页面链接对象
                        var pagesSplitObj = pagesSplit(config.pageNumber,data.pageTotal);
                        if( pagesSplitObj.endPage!= pagesSplitObj.startPage)
                        {
                            var pageIndex = 0;  // 分页数组序列
                            var currPageIndex = 0;  //当前分页数组下标
                            //第一页
                            if( pagesSplitObj.startPage!=1)
                            {
                                var tmpName = "1";
                                if(pagesSplitObj.startPage!=2)
                                {
                                    tmpName+="...";
                                }
                                data.pages.push({pageName:tmpName,number:1,click:angular.element.proxy(
                                    config.click.method, config.click.context,
                                    {pageNumber:1,
                                        pageSize:config.pageSize,
                                        pageTotal:config.pageTotal,
                                        total:config.total
                                    }),clazz:""});
                            }
                            for (i = pagesSplitObj.startPage; i <=  pagesSplitObj.endPage; i++) {
                                var clazz ="";
                                if(i==config.pageNumber)
                                {
                                    clazz ="active";  //将选中的页面链接对象变灰以及禁止点击
                                }
                                data.pages.push({pageName:i,number:i,click:angular.element.proxy(
                                    config.click.method, config.click.context,
                                    {pageNumber:i,
                                        pageSize:config.pageSize,
                                        //pageTotal:config.pageTotal,
                                        total:config.total
                                    }),clazz:clazz});
                            }//生成页面链接对象
                             //最后一页
                            if( pagesSplitObj.endPage!=data.pageTotal)
                            {
                                var tmpName = data.pageTotal;
                                if(pagesSplitObj.endPage+1!=data.pageTotal)
                                {
                                    tmpName ="..."+tmpName;
                                }
                                data.pages.push({pageName:tmpName,number:data.pageTotal,click:angular.element.proxy(
                                    config.click.method, config.click.context,
                                    {pageNumber:data.pageTotal,
                                        pageSize:config.pageSize,
                                        pageTotal:config.pageTotal,
                                        total:config.total
                                    }),clazz:""});

                            }
                        }

                        data.total = config.total;
                        data.pageSize = config.pageSize;
                        return data;
                    };
                    scope.$watchCollection("[pageNumber,total,pageSize,click]",function(newVal){
                        config.pageSize =parseInt(newVal[2]);
                        config.total = parseInt(newVal[1]);
                        config.pageNumber = parseInt(newVal[0]);
                        config.click = newVal[3];
                        scope.data = getData(config);
                        //$log.info(scope.data);
                    });//监控组件域的变量变化
                    var isolateScope = scope.$new(true);
                    var i = 0;
                    isolateScope.config = config;
                    isolateScope.$watch('config', function(config) {
                        scope.data = getData(config);
                    });//用户通过自定义控制器改变这个控件的一些参数
                    if (!angular.isUndefined(scope["controller"]))//创建用户自定义控制器
                        var controller = $controller(scope["controller"], {$scope:isolateScope});


                }
            };
        }])
});