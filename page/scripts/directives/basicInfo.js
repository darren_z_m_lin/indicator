/**
 * Created by JetBrains WebStorm.
 * User: Darren Z M LIN
 * Date: 13-9-27
 * Time: 下午2:52
 * To change this template use File | Settings | File Templates.
 */
/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("basicInfo", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/basicInfo.html',
                scope: {
                    controller:'@controller'
                },
                link: function (scope, elem, attrs) {
                    var isolateScope = scope.$new(true);
                    isolateScope.config = {}
                    var controller = $controller(scope["controller"], {$scope:isolateScope});
                    isolateScope.$watch("config", function(newVal) {
                        //$log.debug("basic info controller scope changed\n"+newVal);
                        var data = [];
                        angular.forEach(newVal,function(value,key){
                            this.push({key:key,value:value});
                        },data);
                        scope.data = data;
                    });
                }
            };
        }])
});