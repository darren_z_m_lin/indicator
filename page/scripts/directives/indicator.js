/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
define(["directives/ui", "services/CategoryServices"], function (ui) {
    /**
     * 指标显示器标签 创建人:林志敏
     */
    ui.directive("indicator", ["$controller", "cache", "$location","$log",function ($controller,cache, $location,$log) {

        return {
            restrict: 'A',
            replace:true,
            templateUrl: '.././page/scripts/directives/indicator.html',
            scope: {
                controller: "@controller",
                obj:"=obj"
            },
            link: function (scope, elem, attrs) {
                //$log.debug(scope);
                var controller = $controller(scope['controller'], {
                    $scope: scope
                });
            }
        };
    }])
});