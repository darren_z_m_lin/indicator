/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("indicatorGridEditDialog", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/indicatorGridEditDialog.html',
                scope: {
                    title:"@title",
                    id:"@id",
                    data:"=data",
                    save:"&save",
                    dimensions:"=dimensions"
                },
                link: function (scope, elem, attrs) {

                }
            };
        }]);
});