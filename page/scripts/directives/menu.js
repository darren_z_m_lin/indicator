/**
 *菜单directives
 * DATE        AUTHOR    VERSION        DESCRIPTION
 * 2013-09-16   蔡书伟   V13.00.001      新增
 * author 蔡书伟
 */
define(["directives/ui","services/services"],function(ui){
    ui.directive("menu",["$controller","$log","SearchSharedService",function(
        $controller,$log,sharedService
        ){

        return {
                    restrict: 'A',
                    templateUrl: '.././page/scripts/directives/menu.html',
                    scope: {
                        controller: "@controller"
                    },
                    link: function (scope, elem, attrs,controller) {
                       var controller = $controller(scope['controller'],{$scope:scope});


                      //  scope.menusData = scope.cfg.data;
                     // var data =   [{"id":"010ab269-8457-4b22-ac7b-a3de9ec0b070","cateName":"ccc","topCate":[{"id":"010ab269-8457-4b22-ac7b-a3de9ec0b070","cateName":"ccc","topCate":null},{"id":"8f7b4e2a-bc7d-4157-9862-5a98fc03b606","cateName":"dsd","topCate":null},{"id":"8ddd3045-8a02-406e-906b-cd799e060d45","cateName":"用户","topCate":null}]},{"id":"8f7b4e2a-bc7d-4157-9862-5a98fc03b606","cateName":"dsd","topCate":null},{"id":"8ddd3045-8a02-406e-906b-cd799e060d45","cateName":"用户","topCate":null}];
                      //  scope.menusData =   data;
                          //   alert("over！");
					 // scope.edit = controller.edit();
                    }
                };
    }])
});