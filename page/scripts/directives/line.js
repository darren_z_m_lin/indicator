/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","echarts","echarts/config","echarts/chart/line","echarts/chart/bar"], function (ui, echarts,chartConfig) {
    /**
     * 曲线图标签 创建人:林志敏
     */
    ui.directive("line", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                scope:{
                    data:"=data",
                    click:"=click"
                    /**
                     *  [{ "value":102034050607,//当前值,
                     "yoy":"-0.5",//同比比值
                     "mom":"+0.90345"， //环比比值,
                     "din":"1"，
                     "unit":"月"},
                     { "value":102034050607,//当前值,
                     "yoy":"-0.5",//同比比值
                     "mom":"+0.90345"， //环比比值,
                     "din":"2"，
                     "unit":"月"}],  //当前指标7组数据
                     */
                },
                link: function (scope, elem, attrs) {
                    var context = this;
                    var ec = echarts.init(elem[0]);
                    var data;
                   /* if (!angular.isUndefined(scope.click)) {
                        click = scope.click;
                    }*/
                    if (!angular.isUndefined(scope.data)) {
                        data = scope.data;

                        
                        /**
                         * @author 曾繁吉(zengfanji@revenco.com)
                         * 创建自定义BarChart配置信息
                         * drift:0向上，1向下趋势
                         */
                        var buildCustomBarOption = function (xData, yData, drift) {
                            /**
                             * @author 曾繁吉(zengfanji@revenco.com)
                             * 箭头的itemStyle配置信息
                             *
                             */

                            function itemStyleV7(data) {

                                var itemS = {//变化
                                    value:data,
                                    itemStyle: {//变化
                                        normal: {
                                            color :'#eb6654'
                                        },
                                        emphasis: {
                                            color: '#de4e39'
                                        }
                                    }
                                };

                                return itemS;
                            };
                            var dataV7 = yData[6];

                            yData[6] = itemStyleV7(dataV7);
                            var triangleValue = dataV7 - dataV7 * 1 / 8;
                            var triangleSymbolRotate = 0;
                            var lineStyle = {
                                normal: {
                                    color :'#f9d1cc'
                                },
                                emphasis: {
                                    color: '#f9d1cc'
                                }
                            };
                            if (drift == 1) {
                                triangleValue = dataV7 - dataV7 * 1 / 3;
                                triangleSymbolRotate = 180;
                                yData[6].itemStyle = {
                                    normal: {
                                        color :'#5d9547'
                                    },
                                    emphasis: {
                                        color: '#389216'
                                    }
                                };
                                lineStyle = {
                                    normal: {
                                        color :'#cfdfc8'
                                    },
                                    emphasis: {
                                        color: '#cfdfc8'
                                    }
                                };
                            }

                            var option = {

                                grid:{
                                    x:0,
                                    y:0,
                                    x2:0,
                                    y2:15
                                },
                                xAxis : [
                                    {
                                        type : 'category',
                                        data : xData,    //变化
                                        splitLine:{
                                            show:false
                                        },
                                        axisLine : {    // 轴线
                                            //show: false
                                            lineStyle:{
                                                color:'#c4c4c4',
                                                width: 0
                                            }
                                        },
                                        axisLabel:{
                                            margin:1,
                                            textStyle:{
                                                color: '#888888',
                                                fontSize:'10px'

                                            }
                                        }

                                    }
                                ],
                                yAxis : [
                                    {

                                        axisLine : {    // 轴线
                                            show: false
                                        },
                                        axisLabel : {    // 轴标记
                                            show:false
                                        },
                                        splitLine:{
                                            show:false
                                        }


                                    }
                                ],
                                series : [
                                    {
                                        name:'   ',
                                        type:'bar',
                                        itemStyle: {
                                            normal: {
                                                color :'#c5dae5'
                                            },
                                            emphasis: {
                                                color: '#98bfd3'
                                            }
                                        },
                                        data:yData
                                    },
                                    {
                                        type:'line',
                                        data:['-','-','-','-','-','-', {
                                            value: dataV7 - dataV7 * 1 / 4,
                                            symbol: 'rectangle',
                                            symbolSize : 0.9,   // 数据级个性化
                                            symbolRotate:180,
                                            itemStyle: lineStyle
                                        }
                                        ]
                                    },
                                    {
                                        type:'line',
                                        data:['-','-','-','-','-','-', {
                                            value: dataV7 - dataV7 * 1 / 5,
                                            symbol: 'rectangle',
                                            symbolSize :0.9,   // 数据级个性化
                                            symbolRotate:180,
                                            itemStyle: lineStyle
                                        }
                                        ]
                                    },
                                    {
                                        type:'line',
                                        data:['-','-','-','-','-','-', {
                                            value: triangleValue,
                                            symbol: 'triangle',
                                            symbolSize :3,   // 数据级个性化
                                            symbolRotate:triangleSymbolRotate,
                                            itemStyle: lineStyle
                                        }
                                        ]
                                    }
                                ]
                            };

                            return option;
                        };
                        var buildCustomBarOptionParams = {
                            xData:[],
                            yData:[],
                            drift:0
                        };
                        if(angular.isUndefined(data))
                            return;
                        angular.forEach(data, function(item) {
                            buildCustomBarOptionParams.xData.push(item.din + item.unit);
                            buildCustomBarOptionParams.yData.push(parseFloat(item.value));
                        });
                        buildCustomBarOptionParams.drift = 0;
                        if(buildCustomBarOptionParams.xData.length===0||buildCustomBarOptionParams.yData.length===0)
                            return;
                        ec.setOption(buildCustomBarOption(
                            buildCustomBarOptionParams.xData,
                            //["1","2","3","4","5","6","7"],
                            buildCustomBarOptionParams.yData,
                            buildCustomBarOptionParams.drift));

                        ec.on(chartConfig.EVENT.CLICK, function(param) {
                            scope.click.method.call(scope.click.context,param);
                         });
                    }

                }
            };
        }])
});