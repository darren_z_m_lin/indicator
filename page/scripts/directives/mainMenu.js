/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("mainMenu", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/mainMenu.html',
                scope: {
                    controller:'@controller'
                },
                link: function (scope, elem, attrs) {
                    var context = this;
                    var defaultFunc = function() {
                        //$log.info("running in the main menu context");
                    };
                    var isolateScope = scope.$new(true);
                    isolateScope.config = {
                        click:{
                            method:defaultFunc,
                            context:context
                        },
                        favourite:true
                    };
                    var controller = $controller(scope["controller"], {$scope:isolateScope});
                    isolateScope.$watch("config", function(newVal) {
                        //$log.debug("main menu controller scope changed\n"+newVal);
                        var config = {
                            category:angular.element.proxy(newVal.click.method, newVal.click.context,"category"),
                            kpi:angular.element.proxy(newVal.click.method, newVal.click.context,"kpi"),
                            admin:angular.element.proxy(newVal.click.method, newVal.click.context,"admin"),
                            favourite:{click:angular.element.proxy(newVal.click.method, newVal.click.context,"favourite"),selected:"current"},
                            setting:angular.element.proxy(newVal.click.method, newVal.click.context,"setting"),
                            help:angular.element.proxy(newVal.click.method, newVal.click.context,"help"),
                            exit:angular.element.proxy(newVal.click.method, newVal.click.context,"exit")
                        }
                        if(!newVal.favourite)
                            config.favourite.selected="";
                        else
                            config.favourite.selected="current";
                        scope.config = config;
                    },true);

                }
            };
        }])
});