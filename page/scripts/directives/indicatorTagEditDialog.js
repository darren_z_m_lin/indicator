/**
 * 创建人:蔡书伟
 * 日期:2013-11-19
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:蔡书伟
     */
    ui.directive("indicatorTagEditDialog", ["$controller", "$log",
        function ($controller, $log) {

            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/indicatorTagEditDialog.html',
                scope: {
                    controller: "@controller"
                },
                link: function (scope, elem, attrs) {
                    var controller = $controller(scope['controller'],{
                        $scope:scope
                    });
                }
            };
        }]);
});