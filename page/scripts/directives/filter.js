/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui",
    "services/CategoryServices",
    "controllers/CategoryControllers"], function (ui) {
    /**
     * 过滤器标签
     */
    ui.directive("filter", ["$controller", "$log", "$timeout",
        function ($controller, $log, $timeout) {
            var validate = function ($scope) {
                if (angular.isUndefined($scope.cfg))
                    throw("cfg is undefined");
                if (angular.isUndefined($scope.cfg.classes))
                    throw("cfg.classes is undefined");
                if (angular.isUndefined($scope.cfg.actions))
                    throw("cfg.actions is undefined");
                if (angular.isUndefined($scope.cfg.data))
                    throw("cfg.data is undefined");
                if (!angular.isArray($scope.cfg.data))
                    throw("cfg.data is not array");
                if (!angular.isObject($scope.cfg))
                    throw("cfg is not object");
                if (!angular.isObject($scope.cfg.classes))
                    throw("cfg.classes is not object");
                if (!angular.isObject($scope.cfg.actions))
                    throw("cfg.actions is not object");
                angular.forEach($scope.cfg.actions, function (item, key) {
                    if (!angular.isFunction(item))
                        throw("cfg.actions." + key + " is not function");
                });
            };
            return {
                restrict: 'A',
                replace: true,
                templateUrl: '.././page/scripts/directives/filter.html',
                scope: {
                    controller: "@controller"

                },
                link: function (scope, elem, attrs) {
                    //$log.debug(scope);
                    var controller = $controller(scope['controller'], {$scope: scope});
                    scope.$watch("tags", function() {
                        $timeout(function () {
                            //展开关闭过滤器
                            angular.element(".select_filter_trigger").click(function () {
                                $(this).siblings(".filter_container").slideToggle("fast");
                                $(this).siblings(".filter_container_min").slideToggle("fast", function () {
                                    $(this).parent(".select_filter_container").toggleClass("folded");
                                });
                            });

                            //过滤器缩小后下拉选择改变文字
                            angular.element(".filter_list .dropdown-menu a").click(function () {
                                $(this).parents(".dropdown-menu").siblings(".filters").find(".filter_text").text($(this).text());
                            });

                            //过滤器标签选择
                            angular.element(".filter_box .filters").click(function () {
                                $(this).addClass("selected").siblings(".filters").not(this).removeClass("selected");
                            });
                            angular.element(".filter_box .filters .icon-remove").click(function (e) {
                                e.stopPropagation();
                                $(this).parent().removeClass("selected");
                            });

                            //趋势图和信息之间切换
                            angular.element(".chart_info_trigger").click(function () {
                                $(this).toggleClass("chart_mode").siblings(".indicator_info").toggleClass("chart_mode");
                            });

                        });//初始化过滤器
                    });


                }
            };
        }])
});