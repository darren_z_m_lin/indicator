/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/services","services/CategoryServices"], function (ui) {
    /**
     * 分页标签 创建人:林志敏
     */
    ui.directive("recommends", ["$controller", "$log",
        function ($controller, $log) {
            return {
                restrict: 'A',
                replace:true,
                templateUrl: '.././page/scripts/directives/recommends.html',
                scope: {
                    controller:'@controller'

                },
                link: function (scope, elem, attrs) {
                    var context = this;
                    var isolateScope = scope.$new(true);
                    var config = {
                        data:[
                            { "id":"2kdfjdkg-dferjkdf-aerjkalsdfer-kerafer",//指标ID
                                "name":"指标名",//指标名称
                                "unit":"元",//轴数值单位
                                "code":"dsfnosdfg",//指标编码
                                "star":5,//关注度
                                "careStatus":0,//0为没关注，1为已经关注
                                "dimensionTypeX":"time", //x维度类型
                                "dimensionTypeY":"time",//y维度类型
                                "values":[
                                    { "value":102034050607,//当前值,
                                        "yoy":"-0.5",//同比比值
                                        "mom":"+0.90345", //环比比值,
                                        "din":"1",
                                        "unit":"月"},

                                    { "value":102034050607,//当前值,
                                        "yoy":"-0.5",//同比比值
                                        "mom":"+0.90345", //环比比值,
                                        "din":"2",
                                        "unit":"月"}
                                ],  //当前指标7组数据
                                "chartType":"", //图表类型
                                "drift":0//向上，1向下趋势
                            }
                        ]
                    };
                    isolateScope.config = config;
                    isolateScope.$watch('config', function(config) {
                        scope.config = config;
                    });//用户通过自定义控制器改变这个控件的一些参数
                    if (!angular.isUndefined(scope["controller"]))//创建用户自定义控制器
                        var controller = $controller(scope["controller"], {$scope:isolateScope});


                }
            };
        }])
});