/**
  * 创建人:林志敏
  * 日期:2013-9-16
  */
define(["directives/ui", "services/CategoryServices", "controllers/CategoryControllers"], function (ui) {
    /**
     * 过滤器标签
     */
    ui.directive("criteria", ["$controller", "$log", "$timeout",
        function ($controller, $log, $timeout) {

        return {
            restrict: 'A',
            replace: true,
            templateUrl: '.././page/scripts/directives/criteria.html',
            scope: {
                controller: "@controller"
            },
            link: function (scope, elem, attrs) {
                //$log.debug(scope);
                var controller = $controller(scope['controller'], {$scope: scope});



            }
        };
    }])
});