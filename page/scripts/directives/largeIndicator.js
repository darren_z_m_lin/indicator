/**
 * Created by JetBrains WebStorm.
 * User: Darren Z M LIN
 * Date: 13-9-26
 * Time: 上午9:38
 * To change this template use File | Settings | File Templates.
 */
/**
 * 创建人:林志敏
 * 日期:2013-9-16
 */
define(["directives/ui","services/CategoryServices"], function (ui) {
    ui.directive("largeIndicator", ["$controller","$log","SearchSharedService",function ($controller, $log, sharedService) {

        return {
            restrict: 'A',
            templateUrl: '.././page/scripts/directives/largeIndicator.html',
            scope: {
                controller: "@controller"
            },
            link: function (scope, elem, attrs) {
                var isolateScope = scope.$new(true);
                var func = function(item) {
                    //$log.info("come from location context | "+item);
                };
                var context = this;
                /*isolateScope.config = {
                 click:{
                 method:func,
                 context:context
                 },
                 data:{
                 title:"title",
                 code:"code",
                 star:5,
                 current:10000000,
                 yoy:2.3,
                 mom:3.2,
                 unit:"unit"
                 }
                 };//初始化加数据*/
                var controller = $controller(scope["controller"], {$scope:isolateScope});
                isolateScope.$watch("config", function(config) {
                    if (!angular.isUndefined(config)) {
                        scope.config = {
                            bookmark:angular.element.proxy(config.click.method, config.click.context, "关注"),
                            complain:angular.element.proxy(config.click.method, config.click.context, "申告"),
                            data:config.data
                        };
                    }
                });

            }
        };
    }])
});
